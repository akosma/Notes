<?php
/**
 * Notes Application
 *
 * @author Adrian Kosmaczewski <learn@akosma.training>
 * @copyright 2017 Adrian Kosmaczewski
 * @license MIT
 *
 * Copyright © 2017. Adrian Kosmaczewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */

declare(strict_types = 1);

namespace Deployer;

require 'recipe/common.php';

// Configuration

set('repository', 'git@bitbucket.org:akosma/notes.git');
set('git_tty', true); // [Optional] Allocate tty for git on first deployment
set('shared_files', []);
set('shared_dirs', []);
set('writable_dirs', []);
set('allow_anonymous_stats', false);
set('keep_releases', 5);
set('ssh_multiplexing', true);
set('main_url', 'akosma.mobi');
set('dir_path', '/home/akosmatr/www');

// Hosts

host('akosma.mobi')
    ->hostname('akosma.mobi')
    ->user("akosmatr")
    ->stage('production')
    ->set('deploy_path', '{{dir_path}}/{{main_url}}')
    ->set('bin/php', '/usr/local/php71/bin/php');

// Tasks

desc('Deploy your project');
task('deploy', [
    'deploy:prepare',
    'deploy:lock',
    'deploy:release',
    'deploy:update_code',
    'deploy:shared',
    'deploy:writable',
    'deploy:vendors',
    'deploy:clear_paths',
    'deploy:symlink',
    'deploy:unlock',
    'cleanup',
    'success'
]);

desc('Flushes the PHP OPC cache of the server');
task('flush_cache', function () {
    run('curl https://{{main_url}}/flush_cache.php');
});

desc('Run tests in server');
task('test', function () {
    cd('{{release_path}}');
    run('/usr/local/php71/bin/php {{dir_path}}/phpunit.phar', ['timeout' => null, 'tty' => true]);
});

desc('Copy production configuration file');
task('copy_config', function() {
    run('ln -s {{dir_path}}/{{main_url}}.config.ini {{release_path}}/config/config.ini');
});

after('deploy', 'flush_cache');
after('deploy', 'copy_config');
after('rollback', 'flush_cache');

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');
