<?php
/**
 * Notes Application
 *
 * @author    Adrian Kosmaczewski <learn@akosma.training>
 * @copyright 2017 Adrian Kosmaczewski
 * @license   MIT
 *
 * Copyright © 2017. Adrian Kosmaczewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */

declare(strict_types=1);

/**
 * Notes – StringHelper.php
 * Created by akosma
 * Date: 06.09.17 14:52
 */

namespace akosma\notes\helpers;

/**
 * Contains helper functions dealing with strings.
 *
 * @package akosma\notes\helpers
 */
final class StringHelper {
    /**
     * Returns a random string of the specified length.
     *
     * @param int $minLength
     * @param int $maxLength
     *
     * @return string
     */
    static function generateRandomString($minLength = 4, $maxLength = 6): string {
        // Adapted from
        // http://stackoverflow.com/a/4356295/133764
        $characters = "abcdefghijklmnopqrstuvwxyz1234567890";
        $charactersLength = strlen($characters);
        $randomString = '';
        if ($minLength > $maxLength) {
            $temp = $maxLength;
            $maxLength = $minLength;
            $minLength = $temp;
        }
        $len = mt_rand($minLength, $maxLength);
        for ($i = 0; $i < $len; $i++) {
            $randomString .= $characters[ mt_rand(0, $charactersLength - 1) ];
        }

        return $randomString;
    }
}
