<?php
/**
 * Notes Application
 *
 * @author    Adrian Kosmaczewski <learn@akosma.training>
 * @copyright 2017 Adrian Kosmaczewski
 * @license   MIT
 *
 * Copyright © 2017. Adrian Kosmaczewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */

declare(strict_types=1);

/**
 * Notes
 * Created by akosma
 * Date: 25.08.17 14:35
 */

namespace akosma\notes\helpers;

use akosma\notes\controllers\NoteController;
use akosma\notes\controllers\TagController;
use akosma\notes\models\NoteInterface;
use Monolog\Logger;
use Slim\Container;
use Slim\Http\Request;
use Slim\Http\Response;
use \Datetime;

final class Frontend {
    /**
     * @var Container
     */
    private $container;
    /**
     * @var Logger
     */
    private $logger;

    public function __construct(Container $container) {
        $this->container = $container;
        $this->logger = $this->container->get("logger");

        assert($this->container !== null, "The container must not be NULL");
        assert($this->logger !== null, "The logger must not be NULL");
    }

    /**
     * @return \Slim\Container
     */
    public function getContainer(): Container {
        return $this->container;
    }

    /**
     * @return \Monolog\Logger
     */
    public function getLogger(): Logger {
        return $this->logger;
    }

    public function ping(Request $request, Response $response): Response {
        $this->getLogger()->addInfo("Ping received");

        return $this->writeResponse($request, $response, true);
    }

    public function telegram(Request $request, Response $response): Response {
        $secret = $request->getAttribute('secret');
        $stored = $this->getContainer()->get("settings")["telegram"];
        if ($secret === $stored) {
            $parsedBody = $request->getParsedBody();
            $message = $parsedBody["message"]["text"];
            $controller =
                NoteController::createWithContainer($this->getContainer());
            $tags = ["telegram"];
            $controller->createNote($message, $tags);

            return $this->writeResponse($request, $response, null, 200);
        }
        return $this->writeResponse($request, $response, null, 404);
    }

    public function getAllNotes(Request $request, Response $response): Response {
        $this->getLogger()->addInfo("Requesting all notes");
        $controller =
            NoteController::createWithContainer($this->getContainer());
        $notes = $controller->getAllNotes();

        return $this->writeResponse($request, $response, $notes);
    }

    public function getAllNotesForTag(Request $request, Response $response): Response {
        $name = $request->getAttribute('name');
        $this->getLogger()->addInfo("Requesting all notes for tag '$name'");
        $controller =
            TagController::createWithContainer($this->getContainer());
        $notes = $controller->getAllNotesByTag($name);

        return $this->writeResponse($request, $response, $notes);
    }

    public function getNote(Request $request, Response $response): Response {
        $uuid = $request->getAttribute('id');
        $this->getLogger()->addInfo("Requesting note $uuid");
        $controller =
            NoteController::createWithContainer($this->getContainer());
        $note = $controller->getNote($uuid);

        return $this->writeResponse($request, $response, $note);
    }

    public function getAllTags(Request $request, Response $response): Response {
        $this->getLogger()->addInfo("Requesting all tags");
        $controller = TagController::createWithContainer($this->getContainer());
        $tags = $controller->getAllTags();

        return $this->writeResponse($request, $response, $tags);
    }

    public function createNote(Request $request, Response $response): Response {
        $this->getLogger()->addInfo("Creating note");
        $parsedBody = $request->getParsedBody();
        $contents = $parsedBody["contents"];
        $tags = null;
        $uuid = null;
        if (array_key_exists("tags", $parsedBody)) {
            $tags = $parsedBody["tags"];
        }
        if (array_key_exists("id", $parsedBody)) {
            $uuid = $parsedBody["id"];
        }
        $controller =
            NoteController::createWithContainer($this->getContainer());
        $note = $controller->createNote($contents, $tags, $uuid);

        return $this->writeResponse($request, $response, $note, 201);
    }

    public function updateNote(Request $request, Response $response): Response {
        $this->getLogger()->addInfo("Editing note");
        $uuid = $request->getAttribute('id');
        $parsedBody = $request->getParsedBody();
        $contents = $parsedBody["contents"];
        $tags = null;
        if (array_key_exists("tags", $parsedBody)) {
            $tags = $parsedBody["tags"];
        }
        $controller =
            NoteController::createWithContainer($this->getContainer());
        $controller->updateNote($uuid, $contents, $tags);

        return $this->writeResponse($request, $response, $uuid, 204);
    }

    public function mailNote(Request $request, Response $response): Response {
        $uuid = $request->getAttribute('id');
        $this->getLogger()->addInfo("Emailing note $uuid");
        $controller =
            NoteController::createWithContainer($this->getContainer());
        $note = $controller->getNote($uuid);
        $this->sendNoteViaEmail($note);

        return $this->writeResponse($request, $response, $uuid, 204);
    }

    public function trashNote(Request $request, Response $response): Response {
        $this->getLogger()->addInfo("Sending note to trash");
        $uuid = $request->getAttribute('id');
        $controller =
            NoteController::createWithContainer($this->getContainer());
        $controller->trashNote($uuid);

        return $this->writeResponse($request, $response, $uuid, 204);
    }

    public function getTrash(Request $request, Response $response): Response {
        $this->getLogger()->addInfo("Showing the trash");
        $controller =
            NoteController::createWithContainer($this->getContainer());
        $notes = $controller->getTrash();

        return $this->writeResponse($request, $response, $notes);
    }

    public function pinNote(Request $request, Response $response): Response {
        $uuid = $request->getAttribute('id');
        $this->getLogger()->addInfo("Pinning note $uuid");
        $controller =
            NoteController::createWithContainer($this->getContainer());
        $controller->pinNote($uuid);

        return $this->writeResponse($request, $response, $uuid, 204);
    }

    public function unpinNote(Request $request, Response $response): Response {
        $uuid = $request->getAttribute('id');
        $this->getLogger()->addInfo("Unpinning note $uuid");
        $controller =
            NoteController::createWithContainer($this->getContainer());
        $controller->unpinNote($uuid);

        return $this->writeResponse($request, $response, $uuid, 204);
    }

    public function publishNote(Request $request, Response $response): Response {
        $uuid = $request->getAttribute('id');
        $this->getLogger()->addInfo("Publishing note $uuid");
        $controller =
            NoteController::createWithContainer($this->getContainer());
        $slug = $controller->publishNote($uuid);

        // This method returns 200, because 204 means literally "no content"
        // and we actually need the slug in the client side!
        return $this->writeResponse($request, $response, $slug, 200);
    }

    public function unpublishNote(Request $request, Response $response): Response {
        $uuid = $request->getAttribute('id');
        $this->getLogger()->addInfo("Unpublishing note $uuid");
        $controller =
            NoteController::createWithContainer($this->getContainer());
        $controller->unpublishNote($uuid);

        return $this->writeResponse($request, $response, $uuid, 204);
    }

    public function search(Request $request, Response $response): Response {
        $text = $request->getAttribute("text");
        $this->getLogger()->addInfo("Searching for notes with keywords: $text");
        $controller =
            NoteController::createWithContainer($this->getContainer());
        $notes = $controller->search($text);

        return $this->writeResponse($request, $response, $notes);
    }

    public function backup(Request $request, Response $response): Response {
        $this->getLogger()->addInfo("Requested backup of notes in ZIP format");
        $controller =
            NoteController::createWithContainer($this->getContainer());
        $notes = $controller->getAllNotes();
        $trash = $controller->getTrash();
        $backupHelper = BackupHelper::create();
        $filename = $backupHelper->zip($notes, $trash);

        // Adapted from
        // https://www.codeproject.com/Articles/831185/Create-Download-Zip-File-Using-PHP
        header('Content-type: application/zip');
        header('Content-Disposition: attachment; filename="'
            . basename($filename) . '"');
        header("Content-length: " . filesize($filename));
        header("Pragma: no-cache");
        header("Expires: 0");
        ob_clean();
        flush();
        readfile($filename);
        unlink($filename);

        return $response;
    }

    private function sendNoteViaEmail(NoteInterface $note) {
        $emailConfig = $this->getContainer()->get("settings")["email"];
        $smtpConfig = $this->getContainer()->get("settings")["smtp"];
        $mailer = Mailer::create($emailConfig, $smtpConfig);
        $mailer->send($note);
    }

    private function writeResponse(Request $request, Response $response, $obj, int $status = 200): Response {
        $now = new Datetime();
        $message = [
            "response"  => $obj,
            "version"   => "v1",
            "timestamp" => $now->format(Datetime::ISO8601),
        ];

        // By default, the API responds JSON.
        // The \akosma\notes\middleware\Serializer class
        // takes the body and re-serializes it into another format if required.
        return $response->withStatus($status)->withJson($message);
    }
}