<?php
/**
 * Notes Application
 *
 * @author Adrian Kosmaczewski <learn@akosma.training>
 * @copyright 2017 Adrian Kosmaczewski
 * @license MIT
 *
 * Copyright © 2017. Adrian Kosmaczewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */

declare(strict_types=1);

/**
 * Notes
 * Created by akosma
 * Date: 01.09.17 21:54
 */

namespace akosma\notes\helpers\serializers;

use CFPropertyList\CFPropertyList;
use CFPropertyList\CFTypeDetector;

/**
 * Provides Binary Property List serialization.
 *
 * This class depends on the CFPropertyList class family.
 *
 * @package akosma\notes\helpers\serializers
 */
final class BinaryPlistSerializer implements SerializerInterface {
    use SerializerTrait;

    /**
     * BinaryPlistSerializer constructor.
     *
     * Private constructor, please use the ::create() static function instead.
     */
    private function __construct() {
    }

    /**
     * Provides the MIME type for the output of this serializer.
     *
     * @return string
     */
    function responseMimeType(): string {
        return "application/x-plist";
    }

    /**
     * Serializes an array into a Binary Property List.
     *
     * This method takes an associative array or an simple linear array
     * and transforms it into a string, containing the binary plist
     * serialization for it.
     *
     * @param array $obj
     *
     * @return string
     */
    function serialize(array $obj): string {
        $plist = new CFPropertyList();
        $typeDetector = new CFTypeDetector();
        $guessedStructure = $typeDetector->toCFType($obj);
        $plist->add($guessedStructure);

        return $plist->toBinary();
    }
}