<?php
/**
 * Notes Application
 *
 * @author    Adrian Kosmaczewski <learn@akosma.training>
 * @copyright 2017 Adrian Kosmaczewski
 * @license   MIT
 *
 * Copyright © 2017. Adrian Kosmaczewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */

declare(strict_types=1);

/**
 * Notes
 * Created by akosma
 * Date: 01.09.17 21:54
 */

namespace akosma\notes\helpers\serializers;

/**
 * Creates instances of SerializerInterface.
 *
 * This factory creates instances implementing the SerializerInterface
 * which encapsulate the logic required to change the format of a response
 * from JSON (the default) to any other format supported by the application.
 *
 * @package akosma\notes\helpers\serializers
 */
final class SerializerFactory {
    /**
     * SerializerFactory constructor.
     *
     * Private constructor, please use the ::create() static function instead.
     */
    private function __construct() {
    }

    /**
     * Creates a new instance of this class.
     *
     * @return \akosma\notes\helpers\serializers\SerializerFactory
     */
    public static function create(): self {
        return new self();
    }

    /**
     * Returns a SerializerInterface object corresponding to the MIME type
     * passed as parameter.
     *
     * The method accepts four possible values:
     *
     * - text/xml (returns an XmlSerializer instance)
     * - application/xml (returns an XmlPlistSerializer instance)
     * - application/x-plist (returns a BinaryPlistSerializer instance)
     *
     * In all other cases, including "application/json", this method returns
     * a JsonSerializer interface. No exception is thrown if the content type
     * does not match "application/json" – a JsonSerializer will be returned.
     *
     * @param null|string $mimeType
     *
     * @return \akosma\notes\helpers\serializers\SerializerInterface
     */
    public function createSerializer(string $mimeType = null): SerializerInterface {
        switch ($mimeType) {
        case "text/xml":
            return XmlSerializer::create();
        case "application/xml":
            return XmlPlistSerializer::create();
        case "application/x-plist":
            return BinaryPlistSerializer::create();
        }

        return JsonSerializer::create();
    }
}