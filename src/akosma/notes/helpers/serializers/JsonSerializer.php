<?php
/**
 * Notes Application
 *
 * @author Adrian Kosmaczewski <learn@akosma.training>
 * @copyright 2017 Adrian Kosmaczewski
 * @license MIT
 *
 * Copyright © 2017. Adrian Kosmaczewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */

declare(strict_types=1);

/**
 * Notes
 * Created by akosma
 * Date: 01.09.17 21:51
 */

namespace akosma\notes\helpers\serializers;

/**
 * Provides JSON serialization.
 *
 * The API by default responds all requests using JSON. When no
 * "Content-Type" header is provided, or one that is not supported in the
 * application, then the response will always be JSON.
 *
 * @package akosma\notes\helpers\serializers
 */
final class JsonSerializer implements SerializerInterface {
    use SerializerTrait;

    /**
     * JsonSerializer constructor.
     *
     * Do not use, use the ::create() function instead.
     */
    private function __construct() {
    }

    /**
     * Provides the MIME type for the output of this serializer.
     *
     * @return string
     */
    function responseMimeType(): string {
        return "application/json;charset=utf-8";
    }

    /**
     * Serializes an array into a JSON.
     *
     * This method takes an associative array or an simple linear array
     * and transforms it into a string, containing the json
     * serialization for it.
     *
     * @param array $obj
     *
     * @return string
     */
    function serialize(array $obj): string {
        return json_encode($obj);
    }
}
