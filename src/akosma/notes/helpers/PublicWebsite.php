<?php
/**
 * Notes Application
 *
 * @author    Adrian Kosmaczewski <learn@akosma.training>
 * @copyright 2017 Adrian Kosmaczewski
 * @license   MIT
 *
 * Copyright © 2017. Adrian Kosmaczewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */

declare(strict_types=1);

/**
 * Notes – PublicWebsite.php
 * Created by akosma
 * Date: 11.09.17 14:30
 */

namespace akosma\notes\helpers;

use akosma\notes\controllers\NoteController;
use Monolog\Logger;
use Parsedown;
use Slim\Container;
use Slim\Http\Request;
use Slim\Http\Response;

final class PublicWebsite {
    /**
     * @var Container
     */
    private $container;
    /**
     * @var Logger
     */
    private $logger;
    /**
     * @var string
     */
    private $template;

    public function __construct(Container $container) {
        $this->container = $container;
        $this->logger = $this->container->get("logger");
        $this->template = $this->container->get("template");

        assert($this->container !== null, "The container must not be NULL");
        assert($this->logger !== null, "The logger must not be NULL");
        assert($this->template !== null, "The template must not be NULL");
    }

    /**
     * @return string
     */
    public function getTemplate(): string {
        return $this->template;
    }

    /**
     * @return \Slim\Container
     */
    public function getContainer(): Container {
        return $this->container;
    }

    /**
     * @return \Monolog\Logger
     */
    public function getLogger(): Logger {
        return $this->logger;
    }

    public function getAllPublishedNotes(Request $request, Response $response): Response {
        $this->getLogger()->addInfo("Get all published notes");
        $controller =
            NoteController::createWithContainer($this->getContainer());
        $notes = $controller->getAllPublishedNotes();

        $pages = "";
        foreach ($notes as $note) {
            /** @var \akosma\notes\models\Note $note */
            $pages .= '<li><a href="/pub/' . $note->getSlug() . '">'
                . $note->getTitle() . '</a></li>';
        }
        $contents = "<h1>Published Notes</h1><ul>$pages</ul>";
        $html = $this->createPage("Notebook", $contents);

        $response->getBody()->write($html);

        return $response->withStatus(200)
            ->withHeader("Content-Type", "text/html");
    }

    public function getPublishedNote(Request $request, Response $response): Response {
        $slug = $request->getAttribute('slug');
        $this->getLogger()->addInfo("Get published note with slug '$slug'");
        $controller =
            NoteController::createWithContainer($this->getContainer());
        $note = $controller->getNoteFromSlug($slug);

        if (is_null($note)) {
            $response->getBody()->write("File not found");

            return $response->withStatus(404)
                ->withHeader("Content-Type", "text/html");
        }

        $title = $note->getTitle();
        $markdown = new Parsedown();
        $html = $markdown->text($note->getContents());
        $html = $this->createPage($title, $html);
        $response->getBody()->write($html);

        return $response->withStatus(200)
            ->withHeader("Content-Type", "text/html");
    }

    private function createPage(string $title, string $html): string {
        ob_start();
        include $this->getTemplate();
        $page = ob_get_clean();

        return $page;
    }
}