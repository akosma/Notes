<?php
/**
 * Notes Application
 *
 * @author    Adrian Kosmaczewski <learn@akosma.training>
 * @copyright 2017 Adrian Kosmaczewski
 * @license   MIT
 *
 * Copyright © 2017. Adrian Kosmaczewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */

declare(strict_types=1);

/**
 * Notes – BackupHelper.php
 * Created by akosma
 * Date: 07.09.17 21:21
 */

namespace akosma\notes\helpers;

use akosma\notes\exceptions\ZipFileCreationException;
use ZipArchive;

final class BackupHelper {
    private function __construct() {
    }

    public static function create(): self {
        return new self();
    }

    public function zip(array $notes, array $trash): string {
        // Adapted from
        // http://php.net/manual/en/ziparchive.addfromstring.php
        $filename = '/tmp/notes.zip';
        $zip = new ZipArchive;
        $res = $zip->open($filename, ZipArchive::CREATE);
        if ($res === true) {
            // Adapted from
            // http://php.net/manual/en/ziparchive.addemptydir.php
            $zip->addEmptyDir('home');
            foreach ($notes as $note) {
                $id = $note->getId();
                $localname = "home/$id.md";
                $contents = $note->getContents();
                $zip->addFromString($localname, $contents);
            }
            $zip->addEmptyDir('trash');
            foreach ($trash as $deletedNote) {
                $id = $deletedNote->getId();
                $localname = "trash/$id.md";
                $contents = $deletedNote->getContents();
                $zip->addFromString($localname, $contents);
            }

            $zip->close();

            return $filename;
        }
        throw new ZipFileCreationException();
    }
}