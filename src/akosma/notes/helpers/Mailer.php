<?php
/**
 * Notes Application
 *
 * @author    Adrian Kosmaczewski <learn@akosma.training>
 * @copyright 2017 Adrian Kosmaczewski
 * @license   MIT
 *
 * Copyright © 2017. Adrian Kosmaczewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */

declare(strict_types=1);

/**
 * Notes – Mailer.php
 * Created by akosma
 * Date: 08.09.17 21:32
 */

namespace akosma\notes\helpers;

use akosma\notes\exceptions\MailException;
use akosma\notes\models\NoteInterface;
use Parsedown;
use PHPMailer\PHPMailer\PHPMailer;

final class Mailer {
    private $emailConfig;
    private $smtpConfig;

    private function __construct() {
    }

    public static function create(array $emailConfig, array $smtpConfig): self {
        $mailer = new self($emailConfig, $smtpConfig);
        $mailer->smtpConfig = $smtpConfig;
        $mailer->emailConfig = $emailConfig;

        return $mailer;
    }

    public function send(NoteInterface $note): void {
        $mail = new PHPMailer(true);

        //Server settings
        $mail->SMTPDebug = 2;
        $mail->isSMTP();
        $mail->Host = $this->smtpConfig["host"];
        $mail->SMTPAuth = true;
        $mail->Username = $this->smtpConfig["username"];
        $mail->Password = $this->smtpConfig["password"];
        $mail->SMTPSecure = $this->smtpConfig["security"];
        $mail->Port = $this->smtpConfig["port"];
        $mail->WordWrap = 80;

        //Recipients
        $mail->setFrom($this->emailConfig["source"],
            $this->emailConfig["from"]);
        $mail->addAddress($this->emailConfig["destination"],
            $this->emailConfig["to"]);

        $markdown = new Parsedown();
        $contents = $note->getContents();
        $html = $markdown->text($contents);

        //Content
        $mail->isHTML(true);
        $mail->Subject = $note->getTitle();
        $mail->Body = $html;
        $mail->AltBody = $contents;

        $result = $mail->send();
        if ( ! $result) {
            throw new MailException();
        }
    }
}