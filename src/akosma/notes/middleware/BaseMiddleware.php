<?php
/**
 * Notes Application
 *
 * @author Adrian Kosmaczewski <learn@akosma.training>
 * @copyright 2017 Adrian Kosmaczewski
 * @license MIT
 *
 * Copyright © 2017. Adrian Kosmaczewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */

declare(strict_types=1);

/**
 * Notes
 * Created by akosma
 * Date: 02.09.17 12:15
 */

namespace akosma\notes\middleware;

use Monolog\Logger;
use Slim\Container;

/**
 * Base class for all middleware of the API.
 *
 * @package akosma\notes\middleware
 */
abstract class BaseMiddleware {
    /**
     * Slim container passed upon creation.
     *
     * @var Container
     */
    private $container;
    /**
     * The current Monolog logger object.
     *
     * @var Logger
     */
    private $logger;

    /**
     * BaseMiddleware constructor.
     *
     * This constructor requires a Slim container instance with a
     * Monolog logger in the "logger" key.
     *
     * @param \Slim\Container $container
     */
    public function __construct(Container $container) {
        $this->container = $container;
        $this->logger = $this->container->get("logger");
        assert($container !== null);
        assert($this->logger !== null, "The logger cannot be NULL");
    }

    /**
     * Protected method to retrieve the current Monolog logger.
     *
     * @return \Monolog\Logger
     */
    protected function getLogger(): Logger {
        return $this->logger;
    }
}