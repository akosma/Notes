<?php
/**
 * Notes Application
 *
 * @author Adrian Kosmaczewski <learn@akosma.training>
 * @copyright 2017 Adrian Kosmaczewski
 * @license MIT
 *
 * Copyright © 2017. Adrian Kosmaczewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */

declare(strict_types=1);

/**
 * Notes
 * Created by akosma
 * Date: 02.09.17 11:06
 */

namespace akosma\notes\middleware;

use akosma\notes\helpers\serializers\SerializerFactory;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Serializer middleware application.
 *
 * This middleware is used at the end of the handler chain in the API.
 * If the "Content-Type" header of the request is not null and different
 * from "application/json" then the respose is rewritten in its entirety.
 *
 * @package akosma\notes\middleware
 */
final class Serializer extends BaseMiddleware {
    /**
     * Invoked by Slim for every request.
     *
     * @param \Slim\Http\Request  $request
     * @param \Slim\Http\Response $response
     * @param                     $next
     *
     * @return \Slim\Http\Response
     */
    public function __invoke(Request $request, Response $response, $next): Response {
        /** @var \Slim\Http\Response $response */
        $response = $next($request, $response);

        $format = $request->getMediaType();
        if ( ! is_null($format) && $format !== "application/json") {
            $this->getLogger()->addInfo("Media type requested: " . $format);
            $response = $this->rewriteResponse($response, $format);
        }

        return $response;
    }

    /**
     * Rewrites the body of the response into a new format.
     *
     * If the client requires a "Content-Type" header that is different
     * to "application/json" then this method grabs the current response
     * body, deserializes back into an object, and re-serializes it
     * into the new format.
     *
     * @param \Slim\Http\Response $response
     * @param                     $format
     *
     * @return \Slim\Http\Response
     */
    private function rewriteResponse(Response $response, $format): Response {
        /**
         * @var string
         */
        $bodyText = (string)$response->getBody();

        // Take the body of the request and de-serialize it back
        $obj = json_decode($bodyText, true);

        // Use a serializer to re-serialize in a new format
        $factory = SerializerFactory::create();
        $serializer = $factory->createSerializer($format);
        $string = $serializer->serialize($obj);
        $contentType = $serializer->responseMimeType();

        /**
         * @var \Psr\Http\Message\StreamInterface
         */
        $body = $response->getBody();
        $body->rewind();
        $body->write($string);
        $response = $response->withHeader('Content-Type', $contentType);

        return $response;
    }
}