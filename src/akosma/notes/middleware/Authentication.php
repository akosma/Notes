<?php
/**
 * Notes Application
 *
 * @author Adrian Kosmaczewski <learn@akosma.training>
 * @copyright 2017 Adrian Kosmaczewski
 * @license MIT
 *
 * Copyright © 2017. Adrian Kosmaczewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */

declare(strict_types=1);

/**
 * Notes
 * Created by akosma
 * Date: 25.08.17 13:44
 */

namespace akosma\notes\middleware;

use akosma\notes\exceptions\ForbiddenAccessException;
use Slim\Container;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Handles the security of the application.
 *
 * This middleware is setup in /public/api/index.php as blocking access
 * to most routes of the API. It works in a very simple way; it just requires
 * the username and password as part of the constructor, and then it
 * verifies that the HTTP Basic credentials passed by the client are those
 * that have been specified. The current implementation stores the credentials
 * in the /config/config.ini file.
 *
 * @package akosma\notes\middleware
 */
final class Authentication extends BaseMiddleware {
    /**
     * Username to check.
     *
     * @var string
     */
    private $username;
    /**
     * Password to check.
     *
     * @var string
     */
    private $password;

    /**
     * Authentication constructor.
     *
     * @param \Slim\Container $container
     * @param string          $username
     * @param string          $password
     */
    public function __construct(Container $container, string $username, string $password) {
        parent::__construct($container);
        assert($username !== null);
        assert($password !== null);
        $this->username = $username;
        $this->password = $password;
    }

    /**
     * Invoked by Slim for every request.
     *
     * This method retrieves and verifies the contents of the HTTP Basic
     * Auth credentials provided by the client of the API.
     *
     * @param \Slim\Http\Request  $request
     * @param \Slim\Http\Response $response
     * @param                     $next
     *
     * @return \Slim\Http\Response
     * @throws \akosma\notes\exceptions\ForbiddenAccessException
     */
    public function __invoke(Request $request, Response $response, $next): Response {
        $serverParams = $request->getServerParams();
        if (isset($serverParams["PHP_AUTH_USER"])
            && isset($serverParams["PHP_AUTH_PW"])) {
            $user = $serverParams["PHP_AUTH_USER"];
            $password = $serverParams["PHP_AUTH_PW"];
            if ($this->authenticate($user, $password)) {
                $response = $next($request, $response);

                return $response;
            }
        }

        throw new ForbiddenAccessException();
    }

    /**
     * Performs the actual verification of credentials.
     *
     * @param string $username
     * @param string $password
     *
     * @return bool
     */
    private function authenticate(string $username, string $password): bool {
        if ($username === $this->username && $password === $this->password) {
            return true;
        }

        return false;
    }
}
