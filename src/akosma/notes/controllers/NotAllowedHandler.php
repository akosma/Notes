<?php
/**
 * Notes Application
 *
 * @author Adrian Kosmaczewski <learn@akosma.training>
 * @copyright 2017 Adrian Kosmaczewski
 * @license MIT
 *
 * Copyright © 2017. Adrian Kosmaczewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */

declare(strict_types=1);

/**
 * Notes
 * Created by akosma
 * Date: 04.09.17 16:09
 */

namespace akosma\notes\controllers;

use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Handler for not allowed HTTP methods.
 *
 * Whenever a client calls a route with the wrong HTTP method,
 * Slim delegates this handler to return a custom 405 message.
 *
 * The response of this handler is formatted by the serializer
 * middleware before being sent to the client.
 *
 * @package akosma\notes\controllers
 */
final class NotAllowedHandler extends BaseController {
    /**
     * Handles not allowed HTTP methods.
     *
     * The "methods" array parameter contains a list of allowed
     * HTTP methods that can be used for the current route.
     *
     * @param \Slim\Http\Request  $request
     * @param \Slim\Http\Response $response
     * @param array               $methods
     *
     * @return \Slim\Http\Response
     */
    public function __invoke(Request $request, Response $response, array $methods): Response {
        $method = $request->getMethod();
        $this->getLogger()->addError("NotAllowedHandler: " . $method);
        $now = new \Datetime();
        $message = [
            "error"          => "Method Not Allowed: " . $method,
            "version"        => "v1",
            "allowedMethods" => implode(', ', $methods),
            "timestamp"      => $now->format(\Datetime::ISO8601),
        ];

        return $response->withStatus(405)->withJson($message);
    }
}