<?php
/**
 * Notes Application
 *
 * @author Adrian Kosmaczewski <learn@akosma.training>
 * @copyright 2017 Adrian Kosmaczewski
 * @license MIT
 *
 * Copyright © 2017. Adrian Kosmaczewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */

declare(strict_types=1);

/**
 * Notes
 * Created by akosma
 * Date: 25.08.17 13:56
 */

namespace akosma\notes\controllers;

use akosma\notes\exceptions\BaseException;
use akosma\notes\exceptions\ForbiddenAccessException;
use akosma\notes\helpers\serializers\SerializerFactory;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Main exception handler of the application.
 *
 * This class is setup by Slim as the main entry point for all
 * unhandled exceptions in the application. The application features
 * specialized exceptions in the code, and they are simply thrown to
 * signal unexpected situations. This controller will catch them and
 * log them, as well as output them to the user following the requested
 * format by the client.
 *
 * @package akosma\notes\controllers
 */
final class ExceptionController extends BaseController {
    /**
     * Slim exception handler.
     *
     * If the exception is a ForbiddenAccessException instance, the
     * code returns a 401 basic authentication challenge back to the user.
     *
     * If the exception is of type BasicException (the base exception class
     * of this application) then the response status code depends on that
     * particular exception. Otherwise, a 500 error is returned.
     *
     * @param \Slim\Http\Request  $request
     * @param \Slim\Http\Response $response
     * @param \Exception          $exception
     *
     * @return \Slim\Http\Response
     */
    public function __invoke(Request $request, Response $response, \Exception $exception): Response {
        $this->getLogger()->addError("Processing exception: $exception");
        if ($exception instanceof ForbiddenAccessException) {
            $response = $response
                ->withStatus(401)
                ->withHeader("WWW-Authenticate",
                    sprintf('Basic realm="%s"', "Notes"));

            return $this->writeError($request, $response, $exception,
                $exception->getStatusCode());
        }
        if ($exception instanceof BaseException) {
            return $this->writeError($request, $response, $exception,
                $exception->getStatusCode());
        }

        return $this->writeError($request, $response, $exception, 500);
    }

    /**
     * Formats the response to the client.
     *
     * This method reads the MIME type requested by the client and formats
     * the response in whichever format is required.
     *
     * @param \Slim\Http\Request  $request
     * @param \Slim\Http\Response $response
     * @param \Exception          $exception
     * @param                     $status
     *
     * @return \Slim\Http\Response
     */
    private function writeError(Request $request, Response $response, \Exception $exception, $status): Response {
        $error = $exception->getMessage();
        $now = new \Datetime();
        $message = [
            "error"     => $error,
            "version"   => "v1",
            "timestamp" => $now->format(\Datetime::ISO8601),
        ];

        $format = $request->getMediaType();
        $factory = SerializerFactory::create();
        $serializer = $factory->createSerializer($format);
        $string = $serializer->serialize($message);
        $contentType = $serializer->responseMimeType();

        $body = $response->getBody();
        $body->rewind();
        $body->write($string);

        return $response->withStatus($status)
            ->withHeader("Content-Type", $contentType);
    }
}