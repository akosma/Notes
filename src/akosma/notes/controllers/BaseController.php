<?php
/**
 * Notes Application
 *
 * @author Adrian Kosmaczewski <learn@akosma.training>
 * @copyright 2017 Adrian Kosmaczewski
 * @license MIT
 *
 * Copyright © 2017. Adrian Kosmaczewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */

declare(strict_types=1);

/**
 * Notes
 * Created by akosma
 * Date: 24.08.17 17:49
 */

namespace akosma\notes\controllers;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Monolog\Logger;
use Slim\Container;

/**
 * Base class for all controllers.
 *
 * This class provides a factory static method, returning new instances
 * of this class. It takes a Slim container as a parameter and verifies
 * that this container has all the required objects inside. It then provides
 * protected getters for all the subclasses.
 *
 * @package akosma\notes\controllers
 */
abstract class BaseController {
    /**
     * The Slim container passed at construction time.
     *
     * @var Container
     */
    private $container;
    /**
     * The Doctrine entity manager used to access database objects.
     *
     * @var EntityManager
     */
    private $entityManager;
    /**
     * The entity repository used to access Note objects.
     *
     * @var EntityRepository
     */
    private $noteRepository;
    /**
     * The entity repository used to access NoteVersion objects.
     *
     * @var EntityRepository
     */
    private $noteVersionRepository;
    /**
     * The entity repository used to access Tag objects.
     *
     * @var EntityRepository
     */
    private $tagRepository;
    /**
     * The logger of the application.
     *
     * @var Logger
     */
    private $logger;

    /**
     * BaseController constructor.
     *
     * This constructor is private; please use the ::create() static
     * method instead to create new instances. This constructor asserts
     * that the Container passed as parameter has the following fields:
     *
     * - A Doctrine entity manager and its associated note, version and tag
     * repositories.
     * - A Monolog logger
     *
     * @param \Slim\Container $container
     */
    private function __construct(Container $container) {
        $this->container = $container;
        assert($container !== null, "The container cannot be NULL");

        $this->entityManager = $this->container->get("entityManager");
        assert($this->entityManager !== null,
            "The entity manager cannot be NULL");

        $this->logger = $this->container->get("logger");
        assert($this->logger !== null, "The logger cannot be NULL");

        $this->noteRepository =
            $this->entityManager->getRepository("akosma\\notes\\models\\Note");
        assert($this->noteRepository !== null, "The repository cannot be NULL");

        $this->noteVersionRepository =
            $this->entityManager->getRepository("akosma\\notes\\models\\NoteVersion");
        assert($this->noteVersionRepository !== null,
            "The repository of note versions cannot be NULL");

        $this->tagRepository =
            $this->entityManager->getRepository("akosma\\notes\\models\\Tag");
        assert($this->tagRepository !== null,
            "The repository of note versions cannot be NULL");
    }

    /**
     * Creates a new instance of a subclass.
     *
     * The container passed as parameter must have the following objects:
     *
     * - A Doctrine entity manager and its associated note, version and tag
     * repositories.
     * - A Monolog logger
     *
     * @param \Slim\Container $container
     *
     * @return static
     */
    public static function createWithContainer(Container $container) {
        $controller = new static($container);

        return $controller;
    }

    /**
     * Returns the Slim container used to create this object.
     *
     * @return \Slim\Container
     */
    protected function getContainer(): Container {
        return $this->container;
    }

    /**
     * Returns the Doctrine entity manager.
     *
     * @return \Doctrine\ORM\EntityManager
     */
    protected function getEntityManager(): EntityManager {
        return $this->entityManager;
    }

    /**
     * Returns the Doctrine entity repository for Note objects.
     *
     * @return \Doctrine\ORM\EntityRepository
     */
    protected function getNoteRepository(): EntityRepository {
        return $this->noteRepository;
    }

    /**
     * Returns the Doctrine entity repository for NoteVersion objects.
     *
     * @return \Doctrine\ORM\EntityRepository
     */
    protected function getNoteVersionRepository(): EntityRepository {
        return $this->noteVersionRepository;
    }

    /**
     * Returns the Doctrine entity repository for Tag objects.
     *
     * @return \Doctrine\ORM\EntityRepository
     */
    protected function getTagRepository(): EntityRepository {
        return $this->tagRepository;
    }

    /**
     * Returns the Monolog logger for the application.
     *
     * @return \Monolog\Logger
     */
    protected function getLogger(): Logger {
        return $this->logger;
    }
}