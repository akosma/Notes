<?php
/**
 * Notes Application
 *
 * @author Adrian Kosmaczewski <learn@akosma.training>
 * @copyright 2017 Adrian Kosmaczewski
 * @license MIT
 *
 * Copyright © 2017. Adrian Kosmaczewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */

declare(strict_types=1);

/**
 * Notes
 * Created by akosma
 * Date: 04.09.17 16:12
 */

namespace akosma\notes\controllers;

use Slim\Http\Request;
use Slim\Http\Response;

/**
 * PHP error handler of the Slim application.
 *
 * In case of unhandled PHP errors in the application, Slim calls
 * this handler and returns a 500 message to the client.
 *
 * @package akosma\notes\controllers
 */
final class PhpErrorHandler extends BaseController {
    /**
     * Handles PHP errors for Slim.
     *
     * @param \Slim\Http\Request  $request
     * @param \Slim\Http\Response $response
     * @param \Throwable          $error
     *
     * @return \Slim\Http\Response
     */
    public function __invoke(Request $request, Response $response, \Throwable $error): Response {
        $this->getLogger()->addError("PhpErrorHandler" . " – "
            . $error->getMessage()
            . " – " . $error->getTraceAsString());
        $now = new \Datetime();
        $message = [
            "error"     => $error->getMessage(),
            "file"      => $error->getFile(),
            "line"      => $error->getLine(),
            "version"   => "v1",
            "timestamp" => $now->format(\Datetime::ISO8601),
        ];

        return $response->withStatus(500)->withJson($message);
    }
}