<?php
/**
 * Notes Application
 *
 * @author Adrian Kosmaczewski <learn@akosma.training>
 * @copyright 2017 Adrian Kosmaczewski
 * @license MIT
 *
 * Copyright © 2017. Adrian Kosmaczewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */

declare(strict_types=1);

/**
 * Notes
 * Created by akosma
 * Date: 24.08.17 17:48
 */

namespace akosma\notes\controllers;

use akosma\notes\models\Tag;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;

/**
 * Manages all DB interactions about Tags
 *
 * This class wraps all database operations regarding Tag instances.
 *
 * @package akosma\notes\controllers
 */
final class TagController extends BaseController {
    /**
     * Finds, creates if needed and returns Tag objects.
     *
     * This method takes an array of strings and after cleansing
     * will retrieve (and create if needed) the Tag instances. These
     * are then returned in an array to the caller.
     *
     * @param array $tagNames
     *
     * @return array
     */
    public function getOrCreateTags(array $tagNames): array {
        $entityManager = $this->getEntityManager();
        $repo = $this->getTagRepository();
        $tags = [];
        $cleansedTags = $this->cleanseStringArray($tagNames);
        /** @var string $tagName */
        foreach ($cleansedTags as $tagName) {
            $tag = $repo->findOneBy(["name" => $tagName]);
            if (is_null($tag)) {
                $tag = Tag::createWithName($tagName);
                $entityManager->persist($tag);
            }
            $tags[] = $tag;
        }
        $entityManager->flush();

        return $tags;
    }

    /**
     * Returns all tags in the system ordered by name.
     *
     * @return array
     */
    public function getAllTags(): array {
        $tagsObjects = $this->getTagRepository()->findBy([], ["name" => "ASC"]);
        $tags = array_map(function ($tag) {
            /** @var Tag $tag */
            return $tag->getName();
        }, $tagsObjects);

        return $tags;
    }

    /**
     * Returns all the non-deleted notes associated with a tag.
     *
     * If the tag passed as parameter does not exist, the method
     * returns an empty array.
     *
     * @param string $name
     *
     * @return array
     */
    public function getAllNotesByTag(string $name): array {
        $tag = $this->getTagRepository()->findOneBy(["name" => $name]);
        if (is_null($tag)) {
            return [];
        }

        // This code solves the following bug, currently in Doctrine:
        // https://github.com/doctrine/doctrine2/issues/5644
        // It turns out that in the case of ManyToMany relationships,
        // PersistentCollection->matching() does not use Criteria
        // instances properly, and so the SQL query generated is not correct.
        // To solve this, we take the internal PersistentCollection, turn
        // into an array, and we return an ArrayCollection instead, which
        // does not have the problem in the ->matching() method.
        $collection = new ArrayCollection($tag->getNotes()->toArray());
        $criteria = Criteria::create()
            ->where(Criteria::expr()->eq("deleted", false))
            ->orderBy(["pinned" => Criteria::DESC, "lastModificationDate" => Criteria::DESC]);
        $matching = $collection->matching($criteria)->toArray();

        // array_values() is required here, because the
        // array returned by the toArray() function after calling matching()
        // preserves the keys (IDs) of the objects, and instead of a simple
        // array one gets an associative array.
        return array_values($matching);
    }

    /**
     * This method cleanses an array of tag names.
     *
     * It removes duplicata, filters empty strings and trims
     * whitespace from each of the entries.
     *
     * @param array $tagNames
     *
     * @return array
     */
    private function cleanseStringArray(array $tagNames): array {
        $trimmedNames = array_map(function ($name) {
            return trim($name);
        }, $tagNames);
        $filteredNames = array_filter($trimmedNames, function ($name) {
            return strlen($name) > 0;
        });
        $uniqueNames = array_unique($filteredNames);

        return $uniqueNames;
    }
}
