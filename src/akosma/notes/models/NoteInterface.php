<?php
/**
 * Notes Application
 *
 * @author    Adrian Kosmaczewski <learn@akosma.training>
 * @copyright 2017 Adrian Kosmaczewski
 * @license   MIT
 *
 * Copyright © 2017. Adrian Kosmaczewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */

declare(strict_types=1);

/**
 * Notes
 * Created by akosma
 * Date: 24.08.17 15:33
 */

namespace akosma\notes\models;

/**
 * Interface NoteInterface
 *
 * @package akosma\notes\models
 */
interface NoteInterface extends \JsonSerializable {
    /**
     * @return bool
     */
    function isPinned(): bool;

    /**
     * @return mixed
     */
    function getId(): string;

    /**
     * @param \akosma\notes\models\NoteVersion $version
     *
     * @return \akosma\notes\models\NoteInterface
     */
    function addVersion(NoteVersion $version): NoteInterface;

    /**
     * @param $tags
     *
     * @return \akosma\notes\models\NoteInterface
     */
    function setTags(array $tags): NoteInterface;

    /**
     * @return array
     */
    function getVersions(): array;

    /**
     * @return mixed
     */
    function getContents(): string;

    /**
     * @param string $contents
     *
     * @return \akosma\notes\models\NoteInterface
     */
    function setContents(string $contents): NoteInterface;

    /**
     * @param bool $pinned
     *
     * @return \akosma\notes\models\NoteInterface
     */
    function setPinned(bool $pinned): NoteInterface;

    /**
     * @param bool $deleted
     *
     * @return \akosma\notes\models\NoteInterface
     */
    function setDeleted(bool $deleted): NoteInterface;

    /**
     * @return array
     */
    function getTags(): array;

    /**
     * Returns the title of the current note
     *
     * The title of the current note is always the first line of the contents.
     *
     * @return string
     */
    function getTitle(): string;

    /**
     * @return \DateTime
     */
    function getCreationDate(): \DateTime;

    public function getLastModificationDate(): \DateTime;

    /**
     * @return bool
     */
    function isDeleted(): bool;

    public function getSlug();

    public function setSlug(string $slug = null);

    public function isPublished(): bool;
}