<?php
/**
 * Notes Application
 *
 * @author    Adrian Kosmaczewski <learn@akosma.training>
 * @copyright 2017 Adrian Kosmaczewski
 * @license   MIT
 *
 * Copyright © 2017. Adrian Kosmaczewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */

declare(strict_types=1);

/**
 * Notes
 * User: akosma
 * Date: 24.08.17 13:27
 */

namespace akosma\notes\models;

use Doctrine\Common\Collections\ArrayCollection;
use Ramsey\Uuid\Uuid;

/**
 * @Entity
 * @Table(name="notes")
 */
final class Note implements NoteInterface {
    use NoteTrait;
    /**
     * @var string
     * @Id
     * @Column(type="guid")
     */
    private $id;
    /**
     * @var string
     * @Column(type="text")
     */
    private $contents = "";
    /**
     * @var Collection
     * @ManyToMany(targetEntity="\akosma\notes\models\Tag", inversedBy="notes")
     * @JoinTable(name="notes_tags",
     *  joinColumns={
     *      @JoinColumn(name="note_id", referencedColumnName="id")
     *  },
     *  inverseJoinColumns={
     *      @JoinColumn(name="tag_id", referencedColumnName="id")
     *  }
     *  )
     */
    private $tags;
    /**
     * @var ArrayCollection
     * @OneToMany(targetEntity="NoteVersion", mappedBy="note",
     *                                        orphanRemoval=true,
     *                                        indexBy="creationDate")
     */
    private $versions;
    /**
     * @var \DateTime
     * @Column(type="datetime")
     */
    private $creationDate;
    /**
     * @var \DateTime
     * @Column(type="datetime")
     */
    private $lastModificationDate;
    /**
     * @var bool
     * @Column(type="boolean")
     */
    private $pinned = false;
    /**
     * @var bool
     * @Column(type="boolean")
     */
    private $deleted = false;
    /**
     * @var string
     * @Column(type="string", nullable=true)
     */
    private $slug = null;

    /**
     * Note constructor.
     */
    private function __construct() {
        $this->tags = new ArrayCollection();
        $this->versions = new ArrayCollection();
    }

    /**
     * Creates an empty note.
     *
     * @return \akosma\notes\models\NoteInterface
     */
    public static function create(): NoteInterface {
        $note = new Note();
        $note->creationDate = new \DateTime();
        $note->lastModificationDate = new \DateTime();
        $note->id = strtoupper(Uuid::uuid1()->toString());

        return $note;
    }

    /**
     * Creates a note with the specified contents, without tags.
     *
     * @param string $contents
     *
     * @return \akosma\notes\models\NoteInterface
     */
    public static function createWithContents(string $contents, array $tags = null, string $uuid = null): NoteInterface {
        $note = self::create();
        $note->setContents($contents);
        if ($tags) {
            $note->setTags($tags);
        }
        if ($uuid) {
            $note->setId(strtoupper($uuid));
        }

        return $note;
    }

    /**
     * @param \akosma\notes\models\NoteVersion $version
     *
     * @return \akosma\notes\models\NoteInterface
     */
    public function addVersion(NoteVersion $version): NoteInterface {
        $this->versions->add($version);

        return $this;
    }

    /**
     * @return mixed
     */
    public function getContents(): string {
        return $this->contents;
    }

    /**
     * @param mixed $contents
     *
     * @return \akosma\notes\models\NoteInterface
     */
    public function setContents(string $contents): NoteInterface {
        $this->lastModificationDate = new \DateTime();
        $this->contents = $contents;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreationDate(): \DateTime {
        return $this->creationDate;
    }

    /**
     * @return mixed
     */
    public function getId(): string {
        return strtoupper($this->id);
    }

    /**
     * @return \DateTime
     */
    public function getLastModificationDate(): \DateTime {
        return $this->lastModificationDate;
    }

    /**
     * @return string
     */
    public function getSlug() {
        return $this->slug;
    }

    /**
     * @param string $slug
     */
    public function setSlug(string $slug = null) {
        $this->slug = $slug;
    }

    /**
     * @return array
     */
    public function getTags(): array {
        return $this->tags->toArray();
    }

    /**
     * @param mixed $newTags
     *
     * @return \akosma\notes\models\NoteInterface
     */
    public function setTags(array $newTags): NoteInterface {
        $this->lastModificationDate = new \DateTime();
        // First, unset the current tags from this note
        /** @var Tag $tag */
        foreach ($this->tags as $tag) {
            $tag->removeNote($this);
        }

        $this->tags->clear();

        /** @var Tag $tag */
        foreach ($newTags as $tag) {
            $this->tags->add($tag);
            $tag->addNote($this);
        }

        return $this;
    }

    /**
     * @return array
     */
    public function getVersions(): array {
        return $this->versions->toArray();
    }

    /**
     * @return bool
     */
    public function isDeleted(): bool {
        return $this->deleted;
    }

    /**
     * @param bool $deleted
     *
     * @return \akosma\notes\models\NoteInterface
     */
    public function setDeleted(bool $deleted): NoteInterface {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * @return bool
     */
    public function isPinned(): bool {
        return $this->pinned;
    }

    /**
     * @param bool $pinned
     *
     * @return \akosma\notes\models\NoteInterface
     */
    public function setPinned(bool $pinned): NoteInterface {
        $this->pinned = $pinned;

        return $this;
    }

    public function isPublished(): bool {
        return $this->slug !== null;
    }

    public function jsonSerialize() {
        $tags = array_map(function ($tag) {
            /** @var Tag $tag */
            return trim($tag->getName());
        }, $this->getTags());
        $arr = [
            "id"                   => $this->getId(),
            "title"                => $this->getTitle(),
            "contents"             => $this->getContents(),
            "creationDate"         => $this->getCreationDate()
                ->format(DATE_ISO8601),
            "lastModificationDate" => $this->getLastModificationDate()
                ->format(DATE_ISO8601),
            "tags"                 => $tags,
            "published"            => $this->getSlug(),
            "pinned"               => $this->isPinned(),
            "versions"             => count($this->getVersions()),
        ];

        return $arr;
    }

    /**
     * @param string $uuid
     *
     * @return \akosma\notes\models\NoteInterface
     */
    private function setId(string $uuid): NoteInterface {
        $this->id = strtoupper(Uuid::fromString($uuid)->toString());

        return $this;
    }
}
