<?php
/**
 * Notes Application
 *
 * @author Adrian Kosmaczewski <learn@akosma.training>
 * @copyright 2017 Adrian Kosmaczewski
 * @license MIT
 *
 * Copyright © 2017. Adrian Kosmaczewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */

declare(strict_types=1);

/**
 * Notes
 * User: akosma
 * Date: 24.08.17 13:36
 */

namespace akosma\notes\models;

use Ramsey\Uuid\Uuid;

/**
 * @Entity
 * @Table(name="versions")
 */
final class NoteVersion {
    use NoteTrait;
    /**
     * @var int
     * @Id
     * @Column(type="integer")
     * @GeneratedValue
     */
    private $id = -1;
    /**
     * @var Note
     * @ManyToOne(targetEntity="Note", inversedBy="versions")
     */
    private $note;
    /**
     * @var \DateTime
     * @Column(type="datetime")
     * @GeneratedValue
     */
    private $creationDate;
    /**
     * @var string
     * @Column(type="text")
     */
    private $contents;

    private function __construct() {
    }

    public static function createFromNote(NoteInterface $note): NoteVersion {
        $version = new NoteVersion();
        $note->addVersion($version);
        $version->note = $note;
        $version->creationDate = $note->getLastModificationDate();
        $version->contents = $note->getContents();

        return $version;
    }

    /**
     * @return \akosma\notes\models\NoteInterface
     */
    public function getNote(): NoteInterface {
        return $this->note;
    }

    /**
     * @return mixed
     */
    public function getNoteId(): string {
        return $this->getNote()->getId();
    }

    /**
     * @return int
     */
    public function getId(): int {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getCreationDate(): \DateTime {
        return $this->creationDate;
    }

    /**
     * @return string
     */
    public function getContents(): string {
        return $this->contents;
    }
}
