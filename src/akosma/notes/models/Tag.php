<?php
/**
 * Notes Application
 *
 * @author Adrian Kosmaczewski <learn@akosma.training>
 * @copyright 2017 Adrian Kosmaczewski
 * @license MIT
 *
 * Copyright © 2017. Adrian Kosmaczewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */

declare(strict_types=1);

/**
 * Notes
 * Created by akosma
 * Date: 24.08.17 17:23
 */

namespace akosma\notes\models;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * @Entity
 * @Table(name="tags")
 */
final class Tag implements \JsonSerializable {
    /**
     * ID of the current instance.
     *
     * @var int
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue(strategy="AUTO")
     **/
    private $id = -1;
    /**
     * @var string
     * @Column(type="string")
     */
    private $name;
    /**
     * @var ArrayCollection
     * @ManyToMany(targetEntity="\akosma\notes\models\Note", mappedBy="tags")
     */
    private $notes;

    private function __construct() {
        $this->notes = new ArrayCollection();
    }

    public static function createWithName(string $name): Tag {
        $tag = new Tag();
        $tag->name = $name;

        return $tag;
    }

    /**
     * @return string
     */
    public function getName(): string {
        return $this->name;
    }

    /**
     * @return array
     */
    public function getNotes(): Collection {
        return $this->notes;
    }

    public function addNote(NoteInterface $note) {
        $this->notes->add($note);
    }

    public function removeNote(NoteInterface $note) {
        $this->notes->removeElement($note);
    }

    /**
     * @return int
     */
    public function getId(): int {
        return $this->id;
    }

    public function jsonSerialize() {
        $arr = [
            "id"   => $this->getId(),
            "name" => $this->getName(),
        ];

        return $arr;
    }
}
