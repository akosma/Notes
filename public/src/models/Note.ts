/*
 * Notes Application
 *
 * @author Adrian Kosmaczewski <learn@akosma.training>
 * @copyright 2017 Adrian Kosmaczewski
 * @license MIT
 *
 * Copyright © 2017. Adrian Kosmaczewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */

export class Note {
    private _id: string;
    private _contents: string;
    private _tags: string[];
    private _creationDate: string;
    private _lastModificationDate: string;
    private _title: string;
    private _createdLocally: boolean = true;
    private _published: string = null;
    private _pinned: boolean = false;

    constructor(json: object) {
        this._id = json["id"];
        this._title = json["title"];
        this._contents = json["contents"];
        this._lastModificationDate = json["lastModificationDate"];
        this._creationDate = json["creationDate"];
        this._tags = json["tags"];
    }

    get pinned(): boolean {
        return this._pinned;
    }

    set pinned(value: boolean) {
        this._pinned = value;
    }

    get lastModificationDate(): string {
        return this._lastModificationDate;
    }

    set lastModificationDate(value: string) {
        this._lastModificationDate = value;
    }

    get published(): string {
        return this._published;
    }

    set published(value: string) {
        this._published = value;
    }

    get creationDate(): string {
        return this._creationDate;
    }

    set creationDate(value: string) {
        this._creationDate = value;
    }

    get createdLocally(): boolean {
        return this._createdLocally;
    }

    set createdLocally(value: boolean) {
        this._createdLocally = value;
    }

    get title(): string {
        return this._title;
    }

    set title(value: string) {
        this._title = value;
    }

    get id(): string {
        return this._id;
    }

    get contents(): string {
        return this._contents;
    }

    set contents(newContents: string) {
        this._contents = newContents;
    }

    get tags(): string[] {
        return this._tags;
    }

    set tags(newTags: string[]) {
        this._tags = newTags;
    }

    public toJSON() {
        return {
            "id": this.id,
            "title": this.title,
            "contents": this.contents,
            "creationDate": this.creationDate,
            "lastModificationDate": this.lastModificationDate,
            "published": this.published,
            "tags": this.tags,
            "pinned": this.pinned
        };
    }
}
