"use strict";
/*
 * Notes Application
 *
 * @author Adrian Kosmaczewski <learn@akosma.training>
 * @copyright 2017 Adrian Kosmaczewski
 * @license MIT
 *
 * Copyright © 2017. Adrian Kosmaczewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */
Object.defineProperty(exports, "__esModule", { value: true });
var Note = /** @class */ (function () {
    function Note(json) {
        this._createdLocally = true;
        this._published = null;
        this._pinned = false;
        this._id = json["id"];
        this._title = json["title"];
        this._contents = json["contents"];
        this._lastModificationDate = json["lastModificationDate"];
        this._creationDate = json["creationDate"];
        this._tags = json["tags"];
    }
    Object.defineProperty(Note.prototype, "pinned", {
        get: function () {
            return this._pinned;
        },
        set: function (value) {
            this._pinned = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Note.prototype, "lastModificationDate", {
        get: function () {
            return this._lastModificationDate;
        },
        set: function (value) {
            this._lastModificationDate = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Note.prototype, "published", {
        get: function () {
            return this._published;
        },
        set: function (value) {
            this._published = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Note.prototype, "creationDate", {
        get: function () {
            return this._creationDate;
        },
        set: function (value) {
            this._creationDate = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Note.prototype, "createdLocally", {
        get: function () {
            return this._createdLocally;
        },
        set: function (value) {
            this._createdLocally = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Note.prototype, "title", {
        get: function () {
            return this._title;
        },
        set: function (value) {
            this._title = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Note.prototype, "id", {
        get: function () {
            return this._id;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Note.prototype, "contents", {
        get: function () {
            return this._contents;
        },
        set: function (newContents) {
            this._contents = newContents;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Note.prototype, "tags", {
        get: function () {
            return this._tags;
        },
        set: function (newTags) {
            this._tags = newTags;
        },
        enumerable: true,
        configurable: true
    });
    Note.prototype.toJSON = function () {
        return {
            "id": this.id,
            "title": this.title,
            "contents": this.contents,
            "creationDate": this.creationDate,
            "lastModificationDate": this.lastModificationDate,
            "published": this.published,
            "tags": this.tags,
            "pinned": this.pinned
        };
    };
    return Note;
}());
exports.Note = Note;
