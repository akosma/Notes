"use strict";
/*
 * Notes Application
 *
 * @author Adrian Kosmaczewski <learn@akosma.training>
 * @copyright 2017 Adrian Kosmaczewski
 * @license MIT
 *
 * Copyright © 2017. Adrian Kosmaczewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */
Object.defineProperty(exports, "__esModule", { value: true });
var PreviewPane = /** @class */ (function () {
    function PreviewPane() {
        var _this = this;
        this._delegate = null;
        // Courtesy of
        // https://stackoverflow.com/a/6217551/133764
        this.createDelayedCallback = function (action) {
            var callcount = 0;
            var delayAction = function (action, time) {
                var expectcallcount = callcount;
                var delay = function () {
                    if (callcount === expectcallcount) {
                        action();
                    }
                };
                setTimeout(delay, time);
            };
            return function (eventtrigger) {
                ++callcount;
                delayAction(action, 1200);
            };
        };
        var config = {
            name: 'myForm',
            fields: [{
                    name: 'contents',
                    type: 'text'
                }, {
                    name: 'tags',
                    type: 'text'
                }, {
                    name: 'published',
                    type: 'checkbox'
                }],
            tabs: [{
                    id: 'editTab', caption: 'Edit', onClick: function () {
                        if (_this.note) {
                            _this.contentsTextarea.val(_this.note.contents);
                            _this.contentsTextarea.focus();
                        }
                    }
                }, {
                    id: 'infoTab', caption: 'Tags', onClick: function () {
                        var field = _this.tagsField;
                        var tags = _this.note.tags.join(", ");
                        field.val(tags);
                        var isPublished = (_this.note.published !== null);
                        _this.publishedCheckbox.prop('checked', isPublished);
                    }
                }, {
                    id: 'previewTab', caption: 'Preview', disabled: true
                }, {
                    id: 'infoVersions', caption: 'Versions', disabled: true
                }]
        };
        this._form = $('#myForm').w2form(config);
        w2ui.layout.content('preview', this._form);
    }
    Object.defineProperty(PreviewPane.prototype, "delegate", {
        get: function () {
            return this._delegate;
        },
        set: function (value) {
            this._delegate = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PreviewPane.prototype, "form", {
        get: function () {
            return this._form;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PreviewPane.prototype, "contentsTextarea", {
        get: function () {
            var _this = this;
            if (!this._contentsTextarea) {
                this._contentsTextarea = $('#contents');
                // Bind keystrokes CTRL+S (100) and CMD+S (115) to save contents
                this._contentsTextarea.keypress(function (event) {
                    var meta = event.metaKey;
                    var ctrl = event.ctrlKey;
                    var which = event.charCode;
                    if (which === 115 && (meta || ctrl)) {
                        event.preventDefault();
                        _this.note.contents = _this.contentsTextarea.val().toString();
                        _this.delegate.onPreviewPaneFormChange();
                    }
                });
                var action = function () {
                    _this.note.contents = _this.contentsTextarea.val().toString();
                    _this.delegate.onPreviewPaneFormChange();
                };
                // Courtesy of
                // https://stackoverflow.com/a/14029861/133764
                var area = document.getElementById("contents");
                if (area["addEventListener"]) {
                    // For Chrome, Safari, Firefox…
                    area["addEventListener"]('input', this.createDelayedCallback(action), false);
                }
                else if (area["attachEvent"]) {
                    // For IE
                    area["attachEvent"]('onpropertychange', this.createDelayedCallback(action));
                }
            }
            return this._contentsTextarea;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PreviewPane.prototype, "tagsField", {
        get: function () {
            var _this = this;
            if (!this._tagsField) {
                this._tagsField = $('#tags');
                var action = function () {
                    _this.note.tags = _this.tagsField.val().toString().split(",");
                    _this.delegate.onPreviewPaneFormChange();
                };
                var field = document.getElementById("tags");
                field.onchange = this.createDelayedCallback(action);
                field.onkeypress = this.createDelayedCallback(action);
                field.onpaste = this.createDelayedCallback(action);
                field.oninput = this.createDelayedCallback(action);
            }
            return this._tagsField;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PreviewPane.prototype, "publishedCheckbox", {
        get: function () {
            var _this = this;
            if (!this._publishedCheckbox) {
                this._publishedCheckbox = $('#published');
                this._publishedCheckbox.on('click', function (event) {
                    var checked = _this.publishedCheckbox.prop('checked');
                    _this.delegate.onPublishedStatusChanged(checked);
                });
            }
            return this._publishedCheckbox;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PreviewPane.prototype, "note", {
        get: function () {
            return this._note;
        },
        set: function (value) {
            this._note = value;
            var tabs = this.form.tabs;
            this.contentsTextarea.val(value.contents);
            var tags = value.tags.join(", ");
            this.tagsField.val(tags);
            var isPublished = (value.published !== null);
            this.publishedCheckbox.prop('checked', isPublished);
            tabs.click("editTab");
        },
        enumerable: true,
        configurable: true
    });
    PreviewPane.prototype.show = function () {
        w2ui.layout.show('preview', true);
    };
    PreviewPane.prototype.hide = function () {
        w2ui.layout.hide('preview', true);
    };
    return PreviewPane;
}());
exports.PreviewPane = PreviewPane;
