"use strict";
/*
 * Notes Application
 *
 * @author Adrian Kosmaczewski <learn@akosma.training>
 * @copyright 2017 Adrian Kosmaczewski
 * @license MIT
 *
 * Copyright © 2017. Adrian Kosmaczewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */
Object.defineProperty(exports, "__esModule", { value: true });
var List_1 = require("../models/List");
var Sidebar = /** @class */ (function () {
    function Sidebar() {
        var _this = this;
        this._delegate = null;
        var config = {
            name: 'sidebar',
            nodes: [{
                    id: 'general', text: 'General', group: true, collapsible: false, expanded: true, nodes: [
                        { id: 'home', text: 'All Notes', icon: 'fa fa-book', selected: true },
                        { id: 'trash', text: 'Trash', icon: 'fa fa-trash' }
                    ]
                }, {
                    id: 'tags', text: 'Tags', group: true, collapsible: false, expanded: true, nodes: []
                }],
            onClick: function (event) {
                if (event.node.parent.id === "general") {
                    switch (event.target) {
                        case 'home':
                            _this.delegate.onSidebarHomeSelected();
                            break;
                        case 'trash':
                            _this.delegate.onSidebarTrashSelected();
                            break;
                    }
                }
                else if (event.node.parent.id === "tags") {
                    // We have clicked over tags here
                    var tag = event.node.text;
                    _this.delegate.onSidebarTagSelected(tag);
                }
            }
        };
        this._sidebar = $().w2sidebar(config);
        w2ui.layout.content('left', this._sidebar);
    }
    Object.defineProperty(Sidebar.prototype, "delegate", {
        get: function () {
            return this._delegate;
        },
        set: function (value) {
            this._delegate = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Sidebar.prototype, "sidebar", {
        get: function () {
            return this._sidebar;
        },
        enumerable: true,
        configurable: true
    });
    Sidebar.prototype.select = function (item) {
        switch (item) {
            case List_1.List.Home:
                this.sidebar.select('home');
                break;
            case List_1.List.Trash:
                this.sidebar.select('trash');
        }
    };
    Sidebar.prototype.displayTags = function (response) {
        for (var i = 0, len = response.length; i < len; ++i) {
            var current = response[i];
            var obj = {
                id: i,
                text: current,
                icon: "fa fa-tag"
            };
            this.sidebar.add('tags', obj);
        }
    };
    return Sidebar;
}());
exports.Sidebar = Sidebar;
