/*
 * Notes Application
 *
 * @author Adrian Kosmaczewski <learn@akosma.training>
 * @copyright 2017 Adrian Kosmaczewski
 * @license MIT
 *
 * Copyright © 2017. Adrian Kosmaczewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */

import W2Grid = W2UI.W2Grid;
import {Note} from "../models/Note";
import {GridDelegate} from "./GridDelegate";

export class Grid {
    private _grid: W2Grid;
    private _notes = null;
    private _delegate: GridDelegate | null = null;

    constructor() {
        const config = {
            name: 'grid',
            header: 'All Notes',
            recid: 'id',
            show: {
                toolbar: true,
                toolbarReload: true,
                toolbarColumns: false,
                toolbarAdd: false,
                toolbarDelete: false,
                toolbarSave: false,
                toolbarSearch: false,
                // This one discovered here:
                // https://github.com/vitmalina/w2ui/issues/1600
                // (it is not in the documentation...)
                toolbarInput: true
            },
            multiSelect: false,
            toolbar: {
                items: [
                    {type: 'break'},
                    {type: 'button', id: 'createNoteButton', caption: 'Create Note', icon: 'fa fa-plus-circle'},
                    {type: 'button', id: 'deleteNoteButton', caption: 'Delete Note', icon: 'fa fa-trash'},
                    {type: 'button', id: 'showPublishedButton', caption: 'Show all published', icon: 'fa fa-desktop'},
                    {type: 'button', id: 'backupButton', caption: 'Backup all notes', icon: 'fa fa-download'},
                    {type: 'button', id: 'mailButton', caption: 'Send note via e-mail', icon: 'fa fa-envelope'}
                ]
            },
            columns: [{
                field: 'pinned', caption: '', size: '2%', style: 'text-align: center',
                editable: {type: 'checkbox', style: 'text-align: center'}
            }, {
                field: 'title', caption: 'Title', size: '58%', sortable: true
            }, {
                field: 'lastModificationDate', caption: 'Last Modification', size: '20%', sortable: true,
                render: record => {
                    return window["w2utils"].date(record.lastModificationDate);
                }
            }, {
                field: 'tags', caption: 'Tags', size: '20%', editable: {type: 'text'},
                render: record => {
                    return record.tags.join(", ");
                }
            }],
            sortData: [{
                // This data is used by the "addNote()" method, when triggering the "localSort()" method
                // so that all data is properly sorted by pinned first, then by last modification date.
                field: 'pinned', direction: 'desc'
            }, {
                field: 'lastModificationDate', direction: 'desc'
            }],
            onToolbar: event => {
                // Event triggered when the user clicks on the toolbar
                switch (event.target) {
                    case "createNoteButton":
                        this.delegate.onGridAdd();
                        return;

                    case "deleteNoteButton":
                        event.force = true;
                        event.preventDefault();
                        let selection = this.grid.getSelection();
                        if (selection.length > 0) {
                            let id = selection[0];
                            this.delegate.onGridDelete(id);
                        }
                        return;

                    case "showPublishedButton":
                        this.delegate.onGridShowPublishedButton();
                        return;

                    case "backupButton":
                        this.delegate.onGridBackupButton();
                        return;

                    case "mailButton":
                        this.delegate.onGridMailButton();
                        return;
                }
            },
            onSearch: event => {
                // Triggered when the user clicks "enter" on the search field
                event.preventDefault();
                let searchText = event.searchValue;
                this.delegate.onGridSearch(searchText);
            },
            onChange: event => {
                // Triggered when the user checks a checkbox on the grid
                let record = this.grid.get(event.recid);
                let newValue = event.value_new;
                this.delegate.onGridChange(record, newValue);
            },
            onSelect: event => {
                // Triggered when the user selects an item on the grid
                let record = this.grid.get(event.recid);
                this.delegate.onGridSelect(record);
            },
            onUnselect: event => {
                // Triggered when the user unselects an item on the grid
                let record = this.grid.get(event.recid);
                this.delegate.onGridUnselect(record);
            },
            onReload: event => {
                // Triggered when the user clicks the "reload" button
                this.delegate.onGridReload();
            }
        };
        this._grid = $().w2grid(config);
        w2ui.layout.content('main', this._grid);
    }

    get delegate(): GridDelegate | any {
        return this._delegate;
    }

    set delegate(value: GridDelegate | any) {
        this._delegate = value;
    }

    get grid(): W2UI.W2Grid {
        return this._grid;
    }

    get notes() {
        return this._notes;
    }

    set notes(value) {
        this._notes = value;
        this._grid.records = this._notes;
        this._grid.refresh();
    }

    public addNote(note: Note) {
        this.notes.unshift(note);
        this.grid.refresh();
        this.grid.localSort();
    }

    public selectNone() {
        this.grid.selectNone();
    }

    public selectNote(note: Note) {
        this.grid.select(note.id);
    }

    public setSearchValue(text: string) {
        // This is pure reverse engineering!
        document.getElementById('grid_grid_search_all')["value"] = text;
    }
}
