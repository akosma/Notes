"use strict";
/*
 * Notes Application
 *
 * @author Adrian Kosmaczewski <learn@akosma.training>
 * @copyright 2017 Adrian Kosmaczewski
 * @license MIT
 *
 * Copyright © 2017. Adrian Kosmaczewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */
Object.defineProperty(exports, "__esModule", { value: true });
var Grid = /** @class */ (function () {
    function Grid() {
        var _this = this;
        this._notes = null;
        this._delegate = null;
        var config = {
            name: 'grid',
            header: 'All Notes',
            recid: 'id',
            show: {
                toolbar: true,
                toolbarReload: true,
                toolbarColumns: false,
                toolbarAdd: false,
                toolbarDelete: false,
                toolbarSave: false,
                toolbarSearch: false,
                // This one discovered here:
                // https://github.com/vitmalina/w2ui/issues/1600
                // (it is not in the documentation...)
                toolbarInput: true
            },
            multiSelect: false,
            toolbar: {
                items: [
                    { type: 'break' },
                    { type: 'button', id: 'createNoteButton', caption: 'Create Note', icon: 'fa fa-plus-circle' },
                    { type: 'button', id: 'deleteNoteButton', caption: 'Delete Note', icon: 'fa fa-trash' },
                    { type: 'button', id: 'showPublishedButton', caption: 'Show all published', icon: 'fa fa-desktop' },
                    { type: 'button', id: 'backupButton', caption: 'Backup all notes', icon: 'fa fa-download' },
                    { type: 'button', id: 'mailButton', caption: 'Send note via e-mail', icon: 'fa fa-envelope' }
                ]
            },
            columns: [{
                    field: 'pinned', caption: '', size: '2%', style: 'text-align: center',
                    editable: { type: 'checkbox', style: 'text-align: center' }
                }, {
                    field: 'title', caption: 'Title', size: '58%', sortable: true
                }, {
                    field: 'lastModificationDate', caption: 'Last Modification', size: '20%', sortable: true,
                    render: function (record) {
                        return window["w2utils"].date(record.lastModificationDate);
                    }
                }, {
                    field: 'tags', caption: 'Tags', size: '20%', editable: { type: 'text' },
                    render: function (record) {
                        return record.tags.join(", ");
                    }
                }],
            sortData: [{
                    // This data is used by the "addNote()" method, when triggering the "localSort()" method
                    // so that all data is properly sorted by pinned first, then by last modification date.
                    field: 'pinned', direction: 'desc'
                }, {
                    field: 'lastModificationDate', direction: 'desc'
                }],
            onToolbar: function (event) {
                // Event triggered when the user clicks on the toolbar
                switch (event.target) {
                    case "createNoteButton":
                        _this.delegate.onGridAdd();
                        return;
                    case "deleteNoteButton":
                        event.force = true;
                        event.preventDefault();
                        var selection = _this.grid.getSelection();
                        if (selection.length > 0) {
                            var id = selection[0];
                            _this.delegate.onGridDelete(id);
                        }
                        return;
                    case "showPublishedButton":
                        _this.delegate.onGridShowPublishedButton();
                        return;
                    case "backupButton":
                        _this.delegate.onGridBackupButton();
                        return;
                    case "mailButton":
                        _this.delegate.onGridMailButton();
                        return;
                }
            },
            onSearch: function (event) {
                // Triggered when the user clicks "enter" on the search field
                event.preventDefault();
                var searchText = event.searchValue;
                _this.delegate.onGridSearch(searchText);
            },
            onChange: function (event) {
                // Triggered when the user checks a checkbox on the grid
                var record = _this.grid.get(event.recid);
                var newValue = event.value_new;
                _this.delegate.onGridChange(record, newValue);
            },
            onSelect: function (event) {
                // Triggered when the user selects an item on the grid
                var record = _this.grid.get(event.recid);
                _this.delegate.onGridSelect(record);
            },
            onUnselect: function (event) {
                // Triggered when the user unselects an item on the grid
                var record = _this.grid.get(event.recid);
                _this.delegate.onGridUnselect(record);
            },
            onReload: function (event) {
                // Triggered when the user clicks the "reload" button
                _this.delegate.onGridReload();
            }
        };
        this._grid = $().w2grid(config);
        w2ui.layout.content('main', this._grid);
    }
    Object.defineProperty(Grid.prototype, "delegate", {
        get: function () {
            return this._delegate;
        },
        set: function (value) {
            this._delegate = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Grid.prototype, "grid", {
        get: function () {
            return this._grid;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Grid.prototype, "notes", {
        get: function () {
            return this._notes;
        },
        set: function (value) {
            this._notes = value;
            this._grid.records = this._notes;
            this._grid.refresh();
        },
        enumerable: true,
        configurable: true
    });
    Grid.prototype.addNote = function (note) {
        this.notes.unshift(note);
        this.grid.refresh();
        this.grid.localSort();
    };
    Grid.prototype.selectNone = function () {
        this.grid.selectNone();
    };
    Grid.prototype.selectNote = function (note) {
        this.grid.select(note.id);
    };
    Grid.prototype.setSearchValue = function (text) {
        // This is pure reverse engineering!
        document.getElementById('grid_grid_search_all')["value"] = text;
    };
    return Grid;
}());
exports.Grid = Grid;
