/*
 * Notes Application
 *
 * @author Adrian Kosmaczewski <learn@akosma.training>
 * @copyright 2017 Adrian Kosmaczewski
 * @license MIT
 *
 * Copyright © 2017. Adrian Kosmaczewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */

import W2Sidebar = W2UI.W2Sidebar;
import {SidebarDelegate} from "./SidebarDelegate";
import {List} from "../models/List";

export class Sidebar {
    private _sidebar: W2Sidebar;
    private _delegate: SidebarDelegate | null = null;

    constructor() {
        let config = {
            name: 'sidebar',
            nodes: [{
                id: 'general', text: 'General', group: true, collapsible: false, expanded: true, nodes: [
                    {id: 'home', text: 'All Notes', icon: 'fa fa-book', selected: true},
                    {id: 'trash', text: 'Trash', icon: 'fa fa-trash'}
                ]
            }, {
                id: 'tags', text: 'Tags', group: true, collapsible: false, expanded: true, nodes: []
            }],
            onClick: event => {
                if (event.node.parent.id === "general") {
                    switch (event.target) {
                        case 'home':
                            this.delegate.onSidebarHomeSelected();
                            break;
                        case 'trash':
                            this.delegate.onSidebarTrashSelected();
                            break;
                    }
                }
                else if (event.node.parent.id === "tags") {
                    // We have clicked over tags here
                    let tag = event.node.text;
                    this.delegate.onSidebarTagSelected(tag);
                }
            }
        };
        this._sidebar = $().w2sidebar(config);
        w2ui.layout.content('left', this._sidebar);
    }

    get delegate(): SidebarDelegate | any {
        return this._delegate;
    }

    set delegate(value: SidebarDelegate | any) {
        this._delegate = value;
    }

    get sidebar(): W2UI.W2Sidebar {
        return this._sidebar;
    }

    public select(item: List) {
        switch (item) {
            case List.Home:
                this.sidebar.select('home');
                break;
            case List.Trash:
                this.sidebar.select('trash');
        }
    }

    public displayTags(response) {
        for (let i = 0, len = response.length; i < len; ++i) {
            let current = response[i];
            let obj = {
                id: i,
                text: current,
                icon: "fa fa-tag"
            };
            this.sidebar.add('tags', obj);
        }
    }
}