/*
 * Notes Application
 *
 * @author Adrian Kosmaczewski <learn@akosma.training>
 * @copyright 2017 Adrian Kosmaczewski
 * @license MIT
 *
 * Copyright © 2017. Adrian Kosmaczewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */

import W2Form = W2UI.W2Form;
import W2Tabs = W2UI.W2Tabs;
import {Note} from "../models/Note";
import {PreviewPaneDelegate} from "./PreviewPaneDelegate";

export class PreviewPane {
    private _form: W2Form;
    private _note: Note;
    private _delegate: PreviewPaneDelegate | null = null;
    private _contentsTextarea: JQuery<HTMLElement>;
    private _tagsField: JQuery<HTMLElement>;
    private _publishedCheckbox: JQuery<HTMLElement>;

    // Courtesy of
    // https://stackoverflow.com/a/6217551/133764
    private createDelayedCallback = (action) => {
        let callcount = 0;
        let delayAction = (action, time) => {
            let expectcallcount = callcount;
            let delay = () => {
                if (callcount === expectcallcount) {
                    action();
                }
            };
            setTimeout(delay, time);
        };
        return (eventtrigger) => {
            ++callcount;
            delayAction(action, 1200);
        }
    };

    constructor() {
        let config = {
            name: 'myForm',
            fields: [{
                name: 'contents',
                type: 'text'
            }, {
                name: 'tags',
                type: 'text'
            }, {
                name: 'published',
                type: 'checkbox'
            }],
            tabs: [{
                id: 'editTab', caption: 'Edit', onClick: () => {
                    if (this.note) {
                        this.contentsTextarea.val(this.note.contents);
                        this.contentsTextarea.focus();
                    }
                }
            }, {
                id: 'infoTab', caption: 'Tags', onClick: () => {
                    let field = this.tagsField;
                    let tags = this.note.tags.join(", ");
                    field.val(tags);
                    let isPublished = (this.note.published !== null);
                    this.publishedCheckbox.prop('checked', isPublished);
                }
            }, {
                id: 'previewTab', caption: 'Preview', disabled: true
            }, {
                id: 'infoVersions', caption: 'Versions', disabled: true
            }]
        };
        this._form = $('#myForm').w2form(config);
        w2ui.layout.content('preview', this._form);
    }

    get delegate(): PreviewPaneDelegate | any {
        return this._delegate;
    }

    set delegate(value: PreviewPaneDelegate | any) {
        this._delegate = value;
    }

    get form(): W2UI.W2Form {
        return this._form;
    }

    get contentsTextarea(): JQuery<HTMLElement> {
        if (!this._contentsTextarea) {
            this._contentsTextarea = $('#contents');

            // Bind keystrokes CTRL+S (100) and CMD+S (115) to save contents
            this._contentsTextarea.keypress(event => {
                let meta = event.metaKey;
                let ctrl = event.ctrlKey;
                let which = event.charCode;
                if ( which === 115 && (meta || ctrl) ) {
                    event.preventDefault();
                    this.note.contents = this.contentsTextarea.val().toString();
                    this.delegate.onPreviewPaneFormChange();
                }
            });

            let action = () => {
                this.note.contents = this.contentsTextarea.val().toString();
                this.delegate.onPreviewPaneFormChange();
            };

            // Courtesy of
            // https://stackoverflow.com/a/14029861/133764
            let area = document.getElementById("contents");
            if (area["addEventListener"]) {
                // For Chrome, Safari, Firefox…
                area["addEventListener"]('input', this.createDelayedCallback(action), false);
            } else if (area["attachEvent"]) {
                // For IE
                area["attachEvent"]('onpropertychange', this.createDelayedCallback(action));
            }
        }
        return this._contentsTextarea;
    }

    get tagsField(): JQuery<HTMLElement> {
        if (!this._tagsField) {
            this._tagsField = $('#tags');

            let action = () => {
                this.note.tags = this.tagsField.val().toString().split(",");
                this.delegate.onPreviewPaneFormChange();
            };

            let field = document.getElementById("tags");
            field.onchange = this.createDelayedCallback(action);
            field.onkeypress = this.createDelayedCallback(action);
            field.onpaste = this.createDelayedCallback(action);
            field.oninput = this.createDelayedCallback(action);
        }
        return this._tagsField;
    }

    get publishedCheckbox(): JQuery<HTMLElement> {
        if (!this._publishedCheckbox) {
            this._publishedCheckbox = $('#published');

            this._publishedCheckbox.on('click', (event) => {
                let checked = this.publishedCheckbox.prop('checked');
                this.delegate.onPublishedStatusChanged(checked);
            });
        }
        return this._publishedCheckbox;
    }

    get note(): Note {
        return this._note;
    }

    set note(value: Note) {
        this._note = value;
        let tabs = this.form.tabs as W2Tabs;
        this.contentsTextarea.val(value.contents);
        let tags = value.tags.join(", ");
        this.tagsField.val(tags);
        let isPublished = (value.published !== null);
        this.publishedCheckbox.prop('checked', isPublished);
        tabs.click("editTab");
    }

    public show() {
        w2ui.layout.show('preview', true);
    }

    public hide() {
        w2ui.layout.hide('preview', true);
    }
}
