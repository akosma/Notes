/*
 * Notes Application
 *
 * @author Adrian Kosmaczewski <learn@akosma.training>
 * @copyright 2017 Adrian Kosmaczewski
 * @license MIT
 *
 * Copyright © 2017. Adrian Kosmaczewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */

import uuid = require('uuid');
import W2Layout = W2UI.W2Layout;
import {Sidebar} from "../views/Sidebar";
import {Grid} from "../views/Grid";
import {PreviewPane} from "../views/PreviewPane";
import {Note} from "../models/Note";
import {TagsController} from "./TagsController";
import {NotesController} from "./NotesController";
import {List} from "../models/List";
import {GridDelegate} from "../views/GridDelegate";
import {SidebarDelegate} from "../views/SidebarDelegate";
import {NotesControllerDelegate} from "./NotesControllerDelegate";
import {PreviewPaneDelegate} from "../views/PreviewPaneDelegate";
import {TagsControllerDelegate} from "./TagsControllerDelegate";

/**
 * Main controller of the application.
 *
 * Handles the interaction between all the components in the user interface.
 */
export class ApplicationController
    implements GridDelegate, SidebarDelegate, NotesControllerDelegate, PreviewPaneDelegate, TagsControllerDelegate {

    private _sidebar: Sidebar;
    private _grid: Grid;
    private _previewPane: PreviewPane;
    private _layout: W2Layout;
    private _currentNote: Note;
    private _tagsController = new TagsController($);
    private _notesController = new NotesController($);
    private _currentList: List = List.Home;

    /** Constructor */
    constructor() {
        const appStyle = 'background-color: #F5F6F7; border: 1px solid #dfdfdf; padding: 0px;';
        const editorStyle = 'background-color: white; border: 1px solid #dfdfdf; padding: 0px';

        const config = {
            name: 'layout',
            padding: 0,
            panels: [
                {type: 'left', size: 200, resizable: true, style: appStyle},
                {type: 'main', style: appStyle},
                {type: 'preview', size: '60%', resizable: true, style: editorStyle, hidden: true}
            ]
        };

        this._layout = $('#layout').w2layout(config);
        this._sidebar = new Sidebar();
        this._previewPane = new PreviewPane();
        this._grid = new Grid();

        // Set this controller as delegate
        this._grid.delegate = this;
        this._sidebar.delegate = this;
        this._previewPane.delegate = this;
        this._notesController.delegate = this;
        this._tagsController.delegate = this;

        // Trigger load initial data
        this._tagsController.loadTags();
        this._notesController.load(List.Home, true);
    }

    //region Getters and Setters

    get currentList(): List {
        return this._currentList;
    }

    set currentList(value: List) {
        this._currentList = value;
    }

    get layout(): W2UI.W2Layout {
        return this._layout;
    }

    set layout(value: W2UI.W2Layout) {
        this._layout = value;
    }

    get sidebar(): Sidebar {
        return this._sidebar;
    }

    get grid(): Grid {
        return this._grid;
    }

    get previewPane(): PreviewPane {
        return this._previewPane;
    }

    get tagsController(): TagsController {
        return this._tagsController;
    }

    get notesController(): NotesController {
        return this._notesController;
    }

    get currentNote(): Note {
        return this._currentNote;
    }

    set currentNote(value: Note) {
        this._currentNote = value;
    }

    //endregion

    //region GridDelegate Methods

    public onGridSelect(record): void {
        this.currentNote = record;
        if (this.currentList !== List.Trash) {
            this.previewPane.note = record;
            this.previewPane.show();
        }
    }

    public onGridUnselect(record): void {
        // if (this.currentNote !== null) {
        //     this.previewPane.note = null;
        //     this.previewPane.hide();
        //     this.currentNote = null;
        // }
    }

    public onGridReload(): void {
        this.notesController.load(this.currentList, false);
    }

    public onGridAdd(): void {
        let uuidv4: string = uuid.v4();
        let note = new Note({
            id: uuidv4,
            title: "New Note",
            contents: "New Note\n\nWrite your note here...",
            tags: [],
            creationDate: (new Date()).toISOString(),
            lastModificationDate: (new Date()).toISOString()
        });
        this.notesController.createNote(note);
    }

    public onGridDelete(id: string): void {
        this.previewPane.hide();
        this.currentNote = null;
        this.notesController.trashNote(id);
    }

    public onGridShowPublishedButton(): void {
        // Courtesy of
        // https://stackoverflow.com/a/19851803/133764
        let win = window.open('/pub', '_blank');
        if (win) {
            // Browser has allowed it to be opened
            win.focus();
        } else {
            // Browser has blocked it
            w2popup.open({
                title: 'Cannot open published notes',
                width: 320,
                height: 180,
                body: '<div class="w2ui-centered">Please allow popups for this website or <a href="/pub">click here.</a></div>',
                buttons: '<button class="w2ui-btn" onclick="w2popup.close();">Close</button>'
            });
        }
    }

    public onGridChange(note: Note, pinned: boolean): void {
        this.notesController.pinNote(note, pinned);
    }

    public onGridSearch(search: string): void {
        this.previewPane.hide();
        this.currentNote = null;
        this.currentList = List.Home;
        this.sidebar.select(this.currentList);

        if (search.length === 0) {
            this.notesController.load(this.currentList, true);
        }
        else {
            this.notesController.search(search);
        }
    }

    public onGridBackupButton(): void {
        // Courtesy of
        // https://stackoverflow.com/a/3749395/133764
        document.getElementById('downloadFrame')["src"] = '/api/v1/backup';
    }

    public onGridMailButton(): void {
        if (this.currentNote) {
            this.notesController.mail(this.currentNote.id);
        }
    }

    //endregion

    //region SidebarDelegate Methods

    public onSidebarHomeSelected(): void {
        this.currentList = List.Home;
        this.notesController.load(this.currentList, false);
    }

    public onSidebarTrashSelected(): void {
        this.currentList = List.Trash;
        this.notesController.load(this.currentList, false);
        this.previewPane.hide();
        this.currentNote = null;
    }

    public onSidebarTagSelected(tag: string): void {
        this.currentList = List.Tag;
        this.notesController.load(this.currentList, false, tag);
    }

    //endregion

    //region NotesControllerDelegate Methods

    public onLoadNotesDone(response: Note[], hidePreview: boolean): void {
        this.grid.notes = response;

        if (hidePreview) {
            this.grid.selectNone();
            this.previewPane.hide();
        }

        if (this.currentNote) {
            this.grid.selectNote(this.currentNote);
        }
    }

    public onSearchNotesDone(response: Note[], text: string): void {
        this.grid.notes = response;
        this.grid.setSearchValue(text);
    }

    public onCreateNoteDone(note: Note): void {
        this.currentNote = note;
        this.grid.addNote(note);
        this.grid.selectNote(note);
        this.onGridSelect(note);
    }

    public onSaveNoteDone(): void {
        this.notesController.load(this.currentList, false);
    }

    public onTrashNoteDone(): void {
        this.notesController.load(this.currentList, false);
    }

    public onNotePublished(slug: string): void {
        let message = '<div class="w2ui-centered">You can visit the note <a href="/pub/' + slug + '" target="_blank">here</a>.</div>';
        w2popup.open({
            title: 'The note has been published',
            width: 320,
            height: 180,
            body: message,
            buttons: '<button class="w2ui-btn" onclick="w2popup.close();">Close</button>'
        });
    }

    public onNoteUnpublished(): void {
        w2popup.open({
            title: 'The note has been unpublished',
            width: 320,
            height: 180,
            body: '<div class="w2ui-centered">The previous URL is no longer valid.</div>',
            buttons: '<button class="w2ui-btn" onclick="w2popup.close();">Close</button>'
        });
    }

    public onNotePinned(): void {
        this.notesController.load(this.currentList, false);
    }

    public onNoteUnpinned(): void {
        this.notesController.load(this.currentList, false);
    }

    public onMailNoteDone(): void {
        w2popup.open({
            title: 'The note has been sent via e-mail',
            width: 320,
            height: 180,
            body: '<div class="w2ui-centered">Please check your inbox.</div>',
            buttons: '<button class="w2ui-btn" onclick="w2popup.close();">Close</button>'
        });
    }

    //endregion

    //region PreviewPaneDelegate Methods

    public onPreviewPaneFormChange(): void {
        this.saveCurrentNoteIfNeeded();
    }

    public onPublishedStatusChanged(publish: boolean): void {
        this._notesController.publishNote(this.currentNote, publish);
    }

    //endregion

    //region TagsControllerDelegate Methods

    public onTagsControllerLoadTagsDone(records): void {
        this.sidebar.displayTags(records);
    }

    //endregion

    private saveCurrentNoteIfNeeded() {
        if (this.currentNote) {
            this.currentNote.tags = this.previewPane.note.tags;
            this.currentNote.contents = this.previewPane.note.contents;
            this.notesController.saveNote(this.currentNote);
        }
    }
}
