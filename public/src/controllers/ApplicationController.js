"use strict";
/*
 * Notes Application
 *
 * @author Adrian Kosmaczewski <learn@akosma.training>
 * @copyright 2017 Adrian Kosmaczewski
 * @license MIT
 *
 * Copyright © 2017. Adrian Kosmaczewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */
Object.defineProperty(exports, "__esModule", { value: true });
var uuid = require("uuid");
var Sidebar_1 = require("../views/Sidebar");
var Grid_1 = require("../views/Grid");
var PreviewPane_1 = require("../views/PreviewPane");
var Note_1 = require("../models/Note");
var TagsController_1 = require("./TagsController");
var NotesController_1 = require("./NotesController");
var List_1 = require("../models/List");
/**
 * Main controller of the application.
 *
 * Handles the interaction between all the components in the user interface.
 */
var ApplicationController = /** @class */ (function () {
    /** Constructor */
    function ApplicationController() {
        this._tagsController = new TagsController_1.TagsController($);
        this._notesController = new NotesController_1.NotesController($);
        this._currentList = List_1.List.Home;
        var appStyle = 'background-color: #F5F6F7; border: 1px solid #dfdfdf; padding: 0px;';
        var editorStyle = 'background-color: white; border: 1px solid #dfdfdf; padding: 0px';
        var config = {
            name: 'layout',
            padding: 0,
            panels: [
                { type: 'left', size: 200, resizable: true, style: appStyle },
                { type: 'main', style: appStyle },
                { type: 'preview', size: '60%', resizable: true, style: editorStyle, hidden: true }
            ]
        };
        this._layout = $('#layout').w2layout(config);
        this._sidebar = new Sidebar_1.Sidebar();
        this._previewPane = new PreviewPane_1.PreviewPane();
        this._grid = new Grid_1.Grid();
        // Set this controller as delegate
        this._grid.delegate = this;
        this._sidebar.delegate = this;
        this._previewPane.delegate = this;
        this._notesController.delegate = this;
        this._tagsController.delegate = this;
        // Trigger load initial data
        this._tagsController.loadTags();
        this._notesController.load(List_1.List.Home, true);
    }
    Object.defineProperty(ApplicationController.prototype, "currentList", {
        //region Getters and Setters
        get: function () {
            return this._currentList;
        },
        set: function (value) {
            this._currentList = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ApplicationController.prototype, "layout", {
        get: function () {
            return this._layout;
        },
        set: function (value) {
            this._layout = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ApplicationController.prototype, "sidebar", {
        get: function () {
            return this._sidebar;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ApplicationController.prototype, "grid", {
        get: function () {
            return this._grid;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ApplicationController.prototype, "previewPane", {
        get: function () {
            return this._previewPane;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ApplicationController.prototype, "tagsController", {
        get: function () {
            return this._tagsController;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ApplicationController.prototype, "notesController", {
        get: function () {
            return this._notesController;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ApplicationController.prototype, "currentNote", {
        get: function () {
            return this._currentNote;
        },
        set: function (value) {
            this._currentNote = value;
        },
        enumerable: true,
        configurable: true
    });
    //endregion
    //region GridDelegate Methods
    ApplicationController.prototype.onGridSelect = function (record) {
        this.currentNote = record;
        if (this.currentList !== List_1.List.Trash) {
            this.previewPane.note = record;
            this.previewPane.show();
        }
    };
    ApplicationController.prototype.onGridUnselect = function (record) {
        // if (this.currentNote !== null) {
        //     this.previewPane.note = null;
        //     this.previewPane.hide();
        //     this.currentNote = null;
        // }
    };
    ApplicationController.prototype.onGridReload = function () {
        this.notesController.load(this.currentList, false);
    };
    ApplicationController.prototype.onGridAdd = function () {
        var uuidv4 = uuid.v4();
        var note = new Note_1.Note({
            id: uuidv4,
            title: "New Note",
            contents: "New Note\n\nWrite your note here...",
            tags: [],
            creationDate: (new Date()).toISOString(),
            lastModificationDate: (new Date()).toISOString()
        });
        this.notesController.createNote(note);
    };
    ApplicationController.prototype.onGridDelete = function (id) {
        this.previewPane.hide();
        this.currentNote = null;
        this.notesController.trashNote(id);
    };
    ApplicationController.prototype.onGridShowPublishedButton = function () {
        // Courtesy of
        // https://stackoverflow.com/a/19851803/133764
        var win = window.open('/pub', '_blank');
        if (win) {
            // Browser has allowed it to be opened
            win.focus();
        }
        else {
            // Browser has blocked it
            w2popup.open({
                title: 'Cannot open published notes',
                width: 320,
                height: 180,
                body: '<div class="w2ui-centered">Please allow popups for this website or <a href="/pub">click here.</a></div>',
                buttons: '<button class="w2ui-btn" onclick="w2popup.close();">Close</button>'
            });
        }
    };
    ApplicationController.prototype.onGridChange = function (note, pinned) {
        this.notesController.pinNote(note, pinned);
    };
    ApplicationController.prototype.onGridSearch = function (search) {
        this.previewPane.hide();
        this.currentNote = null;
        this.currentList = List_1.List.Home;
        this.sidebar.select(this.currentList);
        if (search.length === 0) {
            this.notesController.load(this.currentList, true);
        }
        else {
            this.notesController.search(search);
        }
    };
    ApplicationController.prototype.onGridBackupButton = function () {
        // Courtesy of
        // https://stackoverflow.com/a/3749395/133764
        document.getElementById('downloadFrame')["src"] = '/api/v1/backup';
    };
    ApplicationController.prototype.onGridMailButton = function () {
        if (this.currentNote) {
            this.notesController.mail(this.currentNote.id);
        }
    };
    //endregion
    //region SidebarDelegate Methods
    ApplicationController.prototype.onSidebarHomeSelected = function () {
        this.currentList = List_1.List.Home;
        this.notesController.load(this.currentList, false);
    };
    ApplicationController.prototype.onSidebarTrashSelected = function () {
        this.currentList = List_1.List.Trash;
        this.notesController.load(this.currentList, false);
        this.previewPane.hide();
        this.currentNote = null;
    };
    ApplicationController.prototype.onSidebarTagSelected = function (tag) {
        this.currentList = List_1.List.Tag;
        this.notesController.load(this.currentList, false, tag);
    };
    //endregion
    //region NotesControllerDelegate Methods
    ApplicationController.prototype.onLoadNotesDone = function (response, hidePreview) {
        this.grid.notes = response;
        if (hidePreview) {
            this.grid.selectNone();
            this.previewPane.hide();
        }
        if (this.currentNote) {
            this.grid.selectNote(this.currentNote);
        }
    };
    ApplicationController.prototype.onSearchNotesDone = function (response, text) {
        this.grid.notes = response;
        this.grid.setSearchValue(text);
    };
    ApplicationController.prototype.onCreateNoteDone = function (note) {
        this.currentNote = note;
        this.grid.addNote(note);
        this.grid.selectNote(note);
        this.onGridSelect(note);
    };
    ApplicationController.prototype.onSaveNoteDone = function () {
        this.notesController.load(this.currentList, false);
    };
    ApplicationController.prototype.onTrashNoteDone = function () {
        this.notesController.load(this.currentList, false);
    };
    ApplicationController.prototype.onNotePublished = function (slug) {
        var message = '<div class="w2ui-centered">You can visit the note <a href="/pub/' + slug + '" target="_blank">here</a>.</div>';
        w2popup.open({
            title: 'The note has been published',
            width: 320,
            height: 180,
            body: message,
            buttons: '<button class="w2ui-btn" onclick="w2popup.close();">Close</button>'
        });
    };
    ApplicationController.prototype.onNoteUnpublished = function () {
        w2popup.open({
            title: 'The note has been unpublished',
            width: 320,
            height: 180,
            body: '<div class="w2ui-centered">The previous URL is no longer valid.</div>',
            buttons: '<button class="w2ui-btn" onclick="w2popup.close();">Close</button>'
        });
    };
    ApplicationController.prototype.onNotePinned = function () {
        this.notesController.load(this.currentList, false);
    };
    ApplicationController.prototype.onNoteUnpinned = function () {
        this.notesController.load(this.currentList, false);
    };
    ApplicationController.prototype.onMailNoteDone = function () {
        w2popup.open({
            title: 'The note has been sent via e-mail',
            width: 320,
            height: 180,
            body: '<div class="w2ui-centered">Please check your inbox.</div>',
            buttons: '<button class="w2ui-btn" onclick="w2popup.close();">Close</button>'
        });
    };
    //endregion
    //region PreviewPaneDelegate Methods
    ApplicationController.prototype.onPreviewPaneFormChange = function () {
        this.saveCurrentNoteIfNeeded();
    };
    ApplicationController.prototype.onPublishedStatusChanged = function (publish) {
        this._notesController.publishNote(this.currentNote, publish);
    };
    //endregion
    //region TagsControllerDelegate Methods
    ApplicationController.prototype.onTagsControllerLoadTagsDone = function (records) {
        this.sidebar.displayTags(records);
    };
    //endregion
    ApplicationController.prototype.saveCurrentNoteIfNeeded = function () {
        if (this.currentNote) {
            this.currentNote.tags = this.previewPane.note.tags;
            this.currentNote.contents = this.previewPane.note.contents;
            this.notesController.saveNote(this.currentNote);
        }
    };
    return ApplicationController;
}());
exports.ApplicationController = ApplicationController;
