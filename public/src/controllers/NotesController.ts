/*
 * Notes Application
 *
 * @author Adrian Kosmaczewski <learn@akosma.training>
 * @copyright 2017 Adrian Kosmaczewski
 * @license MIT
 *
 * Copyright © 2017. Adrian Kosmaczewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */

import {Note} from "../models/Note";
import {List} from "../models/List";
import {NotesControllerDelegate} from "./NotesControllerDelegate";

/**
 * Network controller for Note requests
 *
 * This class manages all the connections to the backend API
 * to retrieve and save notes.
 */
export class NotesController {
    /**
     * Delegate object, receiving all events generated by the current instance.
     * @type {NotesControllerDelegate | null}
     * @private
     */
    private _delegate: NotesControllerDelegate | null = null;
    private _jquery: JQueryStatic = null;

    constructor(jquery: JQueryStatic) {
        this._jquery = jquery;
    }

    /**
     * Delegate getter.
     *
     * @returns {NotesControllerDelegate | any}
     */
    get delegate(): NotesControllerDelegate | any {
        return this._delegate;
    }

    /**
     * Delegate setter.
     *
     * @param {NotesControllerDelegate | any} value
     */
    set delegate(value: NotesControllerDelegate | any) {
        this._delegate = value;
    }

    /**
     * Sends to the trash the note whose identifier is passed as parameter.
     *
     * @param {string} id
     */
    public trashNote(id: string) {
        let url = "/api/v1/notes/" + id + "/trash";
        this._jquery.ajax(url, {
            method: "PUT",
            contentType: "application/json",
            success: data => {
                this.delegate.onTrashNoteDone();
            }
        });
    }

    /**
     * Saves the note passed as parameter.
     *
     * The `silent` boolean parameter, when set to true, disables the
     * call of the delegate method when done. This can be helpful for
     * "background saves" while the user keeps on doing something else.
     *
     * @param {Note} note
     * @param {boolean} silent
     */
    public saveNote(note: Note, silent: boolean = false) {
        if (note !== null) {
            let method = "PUT";
            let url = '/api/v1/notes/' + note.id;

            this._jquery.ajax(url,{
                method: method,
                contentType: "application/json",
                data: JSON.stringify(note),
                success: data => {
                    if (!silent) {
                        this.delegate.onSaveNoteDone();
                    }
                }
            });
        }
    }

    /**
     * Creates a new note object in the server.
     *
     * @param {Note} note
     */
    public createNote(note: Note) {
        if (note !== null) {
            let method = "POST";
            let url = '/api/v1/notes';

            this._jquery.ajax(url,{
                method: method,
                contentType: "application/json",
                data: JSON.stringify(note),
                success: data => {
                    this.delegate.onCreateNoteDone(data.response);
                }
            });
        }
    }

    public publishNote(note: Note, publish: boolean): void {
        if (note !== null) {
            let method = "PUT";
            let url = '/api/v1/notes/' + note.id;

            if (publish) {
                url += '/publish';
            }
            else {
                url += '/unpublish';
            }

            this._jquery.ajax(url,{
                method: method,
                contentType: "application/json",
                success: data => {
                    if (data) {
                        this.delegate.onNotePublished(data.response);
                    }
                    else {
                        this.delegate.onNoteUnpublished();
                    }
                }
            });
        }
    }

    public pinNote(note: Note, pinned: boolean): void {
        if (note !== null) {
            let method = "PUT";
            let url = '/api/v1/notes/' + note.id;

            if (pinned) {
                url += '/pin';
            }
            else {
                url += '/unpin';
            }

            this._jquery.ajax(url,{
                method: method,
                contentType: "application/json",
                success: data => {
                    if (pinned) {
                        this.delegate.onNotePinned();
                    }
                    else {
                        this.delegate.onNoteUnpinned();
                    }
                }
            });
        }
    }

    /**
     * Loads a list of notes.
     *
     * The list that must be loaded is identified by a `List` enumeration value.
     * When the value passed is `List.Tag` then the `tagId` parameter
     * is required, specifying the ID of the tag.
     *
     * @param {List} target
     * @param hidePreview
     * @param tag
     */
    public load(target: List, hidePreview: boolean, tag: string = "") {
        let url = '/api/v1/notes';
        switch (target) {
            case List.Home:
                url = '/api/v1/notes';
                break;
            case List.Trash:
                url = '/api/v1/trash';
                break;
            case List.Tag:
                url = '/api/v1/tags/' + tag;
                break;
        }
        this._jquery.ajax(url, {
            contentType: "application/json",
            method: 'GET',
            success: data => {
                let response = data['response'];
                this.delegate.onLoadNotesDone(response, hidePreview);
            }
        });
    }

    public search(text: string) {
        let url = '/api/v1/search/' + encodeURIComponent(text);
        this._jquery.ajax(url, {
            contentType: "application/json",
            method: 'GET',
            success: data => {
                let response = data['response'];
                this.delegate.onSearchNotesDone(response, text);
            }
        });
    }

    public mail(id: string) {
        let url = "/api/v1/notes/" + id + "/mail";
        this._jquery.ajax(url, {
            method: "PUT",
            contentType: "application/json",
            success: data => {
                this.delegate.onMailNoteDone();
            }
        });
    }
}