<?php
/**
 * Notes Application
 *
 * @author Adrian Kosmaczewski <learn@akosma.training>
 * @copyright 2017 Adrian Kosmaczewski
 * @license MIT
 *
 * Copyright © 2017. Adrian Kosmaczewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */

declare(strict_types=1);

/**
 * Notes – template.php
 * Created by akosma
 * Date: 08.09.17 21:25
 */
?>
<!DOCTYPE html><!--[if lt IE 7 ]><html class="ie6" lang="en"><![endif]--><!--[if IE 7 ]><html class="ie7" lang="en"><![endif]--><!--[if IE 8 ]><html class="ie8" lang="en"><![endif]--><!--[if IE 9 ]><html class="ie9" lang="en"><![endif]--><!--[if (gte IE 10)|!(IE)]><!--><html lang="en"><!--<![endif]--><head><meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1"><meta charset=utf-8><meta name="viewport" content="width=device-width, initial-scale=1"><meta name="robots" content="noindex"><title><?= $title ?></title><link rel="shortcut icon" type="image/x-icon" sizes="16x16 32x32 64x64" href="/favicon.ico" />
    <link rel="stylesheet" href="/resources/css/published.css" />
</head><body><div class="published-wrap no-ad-size">

<div class="wrapper">
    <div class="app">
        <div class="note">


            <div id="published_content" class="markdown">
                                                                                                                                <?= $html ?>
            </div>


        </div><!-- .note -->


    </div><!-- .app -->
</div>

    <div class="published-top"><span class="logo"><a href="https://github.com/akosma/notes">Published with Notes</a> by <a href="https://akos.ma/">Adrian Kosmaczewski</a></span></div></div><!-- .published-wrap -->
</body></html>
