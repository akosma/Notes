/*
 * Notes Application
 *
 * @author Adrian Kosmaczewski <learn@akosma.training>
 * @copyright 2017 Adrian Kosmaczewski
 * @license MIT
 *
 * Copyright © 2017. Adrian Kosmaczewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */

/*
This helper file is inspired by this article:
https://medium.com/@mmontoya/testing-reactjs-components-with-mocha-webpack-ce7f710d268c

and this gist:
https://gist.github.com/tomazzaman/1e2c71e1afb6a45c1001

The rationale is that jQuery requires a DOM to have an "$.ajax()" method, which
does not exist when running the tests in the command line. We tell then
mocha to load this module _only_ in command line environments, as well as
in PhpStorm, to simulate a browser environment. In the case of the web
runner, this file is not required, since jQuery is loaded by a <script> tag.

PS: I know this is not a really good idea!
https://github.com/tmpvar/jsdom/wiki/Don%27t-stuff-jsdom-globals-onto-the-Node-global
 */

import {JSDOM} from "jsdom";
import * as path from "path";
import * as fs from "fs";
import {ApplicationController} from "../src/controllers/ApplicationController";

// Read an HTML file and load it in JSDOM
let htmlText = fs.readFileSync(path.resolve(__dirname, "", "jsdom.html"));
let dom = new JSDOM(htmlText, {resources: "usable", runScripts: "dangerously"});
let win = dom.window;
global["jQuery"] = global["$"] = require("jquery")(win);

// Load W2UI into the HTML file
let w2uiText = fs.readFileSync(path.resolve(__dirname, "../lib", "w2ui.min.js"));
let script = win.document.createElement('script');
script.textContent = w2uiText.toString();
win.document.body.appendChild(script);

global["w2ui"] = win["w2ui"];

let app = new ApplicationController();
