"use strict";
/*
 * Copyright © 2017. Adrian Kosmaczewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */
Object.defineProperty(exports, "__esModule", { value: true });
var chai_1 = require("chai");
var Note_1 = require("../src/models/Note");
describe("Note class", function () {
    it("can be created", function () {
        var note = new Note_1.Note({
            "contents": "Test"
        });
        chai_1.expect(note).to.be.instanceof(Note_1.Note);
    });
    it("can set and get creation date", function () {
        var note = new Note_1.Note({});
        note.creationDate = "some date";
        chai_1.expect(note.creationDate).to.equal("some date");
    });
    it("can set and get contents", function () {
        var note = new Note_1.Note({
            "contents": "Test"
        });
        chai_1.expect(note.contents).to.equal("Test");
        note.contents = "New contents";
        chai_1.expect(note.contents).to.equal("New contents");
    });
    it("can say whether it's created locally", function () {
        var note = new Note_1.Note({});
        chai_1.expect(note.createdLocally).to.be.true;
        note.createdLocally = false;
        chai_1.expect(note.createdLocally).to.be.false;
    });
    it("can have a title", function () {
        var note = new Note_1.Note({});
        note.title = "Title";
        chai_1.expect(note.title).to.equal("Title");
    });
    it("must have an id", function () {
        var note = new Note_1.Note({});
        chai_1.expect(note.id).not.to.be.null;
    });
    it("can have tags", function () {
        var note = new Note_1.Note({});
        note.tags = ['one', 'two', 'three'];
        chai_1.expect(note.tags).to.have.length(3);
    });
    it("can be converted into JSON", function () {
        var note = new Note_1.Note({});
        note.title = "Title";
        note.tags = ['one', 'two', 'three'];
        note.creationDate = "some date";
        note.contents = "New contents";
        var json = note.toJSON();
        chai_1.expect(json["title"]).to.equal(note.title);
        chai_1.expect(json["tags"]).to.equal(note.tags);
        chai_1.expect(json["contents"]).to.equal(note.contents);
        chai_1.expect(json["creationDate"]).to.equal(note.creationDate);
    });
});
