"use strict";
/*
 * Notes Application
 *
 * @author Adrian Kosmaczewski <learn@akosma.training>
 * @copyright 2017 Adrian Kosmaczewski
 * @license MIT
 *
 * Copyright © 2017. Adrian Kosmaczewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */
Object.defineProperty(exports, "__esModule", { value: true });
var chai_1 = require("chai");
var sinon = require("sinon");
var assert = require("assert");
var NotesController_1 = require("../src/controllers/NotesController");
var List_1 = require("../src/models/List");
var Note_1 = require("../src/models/Note");
describe("NotesController class", function () {
    it("can be created", function () {
        var controller = new NotesController_1.NotesController($);
        chai_1.expect(controller).to.be.instanceOf(NotesController_1.NotesController);
    });
    it("calls delegate when notes are loaded", function () {
        var controller = new NotesController_1.NotesController($);
        var delegate = {
            onLoadNotesDone: function (response, hidesPreview) {
            }
        };
        var data = { 'response': true };
        sinon.stub($, 'ajax').yieldsTo("success", data);
        var mock = sinon.mock(delegate);
        var expectation = mock.expects("onLoadNotesDone").withArgs(true, false).exactly(1);
        controller.delegate = delegate;
        controller.load(List_1.List.Home, false);
        assert($.ajax["calledOnce"]);
        expectation.verify();
    });
    it("calls delegate when trash is loaded", function () {
        var controller = new NotesController_1.NotesController($);
        var delegate = {
            onLoadNotesDone: function (response, hidesPreview) {
            }
        };
        var data = { 'response': true };
        sinon.stub($, 'ajax').yieldsTo("success", data);
        var mock = sinon.mock(delegate);
        var expectation = mock.expects("onLoadNotesDone").withArgs(true, true).exactly(1);
        controller.delegate = delegate;
        controller.load(List_1.List.Trash, true);
        assert($.ajax["calledOnce"]);
        expectation.verify();
    });
    it("calls delegate after trashing note", function () {
        var controller = new NotesController_1.NotesController($);
        var delegate = {
            onTrashNoteDone: function () {
            }
        };
        sinon.stub($, 'ajax').yieldsTo("success");
        var mock = sinon.mock(delegate);
        var expectation = mock.expects("onTrashNoteDone").exactly(1);
        controller.delegate = delegate;
        controller.trashNote("0A6E19AD-34D7-4C00-875A-40F9E7EADD06");
        assert($.ajax["calledOnce"]);
        expectation.verify();
    });
    it("calls delegate after saving note in non-silent mode", function () {
        var controller = new NotesController_1.NotesController($);
        var note = new Note_1.Note({});
        var delegate = {
            onSaveNoteDone: function () {
            }
        };
        sinon.stub($, 'ajax').yieldsTo("success");
        var mock = sinon.mock(delegate);
        var expectation = mock.expects("onSaveNoteDone").exactly(1);
        controller.delegate = delegate;
        controller.saveNote(note);
        assert($.ajax["calledOnce"]);
        expectation.verify();
    });
    it("does not call delegate after saving note in silent mode", function () {
        var controller = new NotesController_1.NotesController($);
        var note = new Note_1.Note({});
        var delegate = {
            onSaveNoteDone: function () {
            }
        };
        sinon.stub($, 'ajax').yieldsTo("success");
        var mock = sinon.mock(delegate);
        var expectation = mock.expects("onSaveNoteDone").never();
        controller.delegate = delegate;
        controller.saveNote(note, true);
        assert($.ajax["calledOnce"]);
        expectation.verify();
    });
    afterEach(function () {
        // When the test either fails or passes, restore the original
        // jQuery ajax function (Sinon.JS also provides tools to help
        // test frameworks automate clean-up like this)
        var restore = $.ajax["restore"];
        if (restore) {
            restore();
        }
    });
});
