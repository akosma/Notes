/*
 * Notes Application
 *
 * @author Adrian Kosmaczewski <learn@akosma.training>
 * @copyright 2017 Adrian Kosmaczewski
 * @license MIT
 *
 * Copyright © 2017. Adrian Kosmaczewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */

import {expect} from "chai";
import {Note} from "../src/models/Note";

describe("Note class", () => {
    it("can be created", () => {
        const note = new Note({
            "contents": "Test"
        });
        expect(note).to.be.instanceof(Note);
    });

    it("can set and get creation date", () => {
        const note = new Note({});
        note.creationDate = "some date";
        expect(note.creationDate).to.equal("some date");
    });

    it("can set and get contents", () => {
        const note = new Note({
            "contents": "Test"
        });
        expect(note.contents).to.equal("Test");
        note.contents = "New contents";
        expect(note.contents).to.equal("New contents");
    });

    it("can have a title", () => {
        const note = new Note({});
        note.title = "Title";
        expect(note.title).to.equal("Title");
    });

    it("must have an id", () => {
        const note = new Note({});
        expect(note.id).not.to.be.null;
    });

    it("can have tags", () => {
        const note = new Note({});
        note.tags = ['one', 'two', 'three'];
        expect(note.tags).to.have.length(3);
    });

    it("can be converted into JSON", () => {
        const note = new Note({});
        note.title = "Title";
        note.tags = ['one', 'two', 'three'];
        note.creationDate = "some date";
        note.contents = "New contents";
        const json = note.toJSON();
        expect(json["title"]).to.equal(note.title);
        expect(json["tags"]).to.equal(note.tags);
        expect(json["contents"]).to.equal(note.contents);
        expect(json["creationDate"]).to.equal(note.creationDate);
    });
});
