/*
 * Notes Application
 *
 * @author Adrian Kosmaczewski <learn@akosma.training>
 * @copyright 2017 Adrian Kosmaczewski
 * @license MIT
 *
 * Copyright © 2017. Adrian Kosmaczewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */

import {expect} from "chai";
import {TagsController} from "../src/controllers/TagsController";
import * as sinon from "sinon";
import * as assert from "assert";

describe("TagController class", () => {
    it("can be created", () => {
        const controller = new TagsController($);
        expect(controller).to.be.instanceof(TagsController);
    });

    it("calls delegate when tags are loaded", () => {
        const controller = new TagsController($);
        const delegate = {
            onTagsControllerLoadTagsDone: data => {
            }
        };

        sinon.stub($, 'ajax').yieldsTo("success", {'data': true});

        let mock = sinon.mock(delegate);
        let expectation = mock.expects("onTagsControllerLoadTagsDone").exactly(1);
        controller.delegate = delegate;
        controller.loadTags();

        assert($.ajax["calledOnce"]);
        expectation.verify();
    });

    afterEach(function () {
        // When the test either fails or passes, restore the original
        // jQuery ajax function (Sinon.JS also provides tools to help
        // test frameworks automate clean-up like this)
        const restore = $.ajax["restore"];
        if (restore) {
            restore();
        }
    });
});
