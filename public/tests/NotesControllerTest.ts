/*
 * Notes Application
 *
 * @author Adrian Kosmaczewski <learn@akosma.training>
 * @copyright 2017 Adrian Kosmaczewski
 * @license MIT
 *
 * Copyright © 2017. Adrian Kosmaczewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */

import {expect} from "chai";
import * as sinon from "sinon";
import * as assert from "assert";
import {NotesController} from "../src/controllers/NotesController";
import {List} from "../src/models/List";
import {Note} from "../src/models/Note";

describe("NotesController class", () => {
    it("can be created", () => {
        const controller = new NotesController($);
        expect(controller).to.be.instanceOf(NotesController);
    });

    it("calls delegate when notes are loaded", () => {
        const controller = new NotesController($);
        const delegate = {
            onLoadNotesDone: (response, hidesPreview) => {
            }
        };

        const data = {'response': true};
        sinon.stub($, 'ajax').yieldsTo("success", data);

        let mock = sinon.mock(delegate);
        let expectation = mock.expects("onLoadNotesDone").withArgs(true, false).exactly(1);
        controller.delegate = delegate;
        controller.load(List.Home, false);

        assert($.ajax["calledOnce"]);
        expectation.verify();
    });

    it("calls delegate when trash is loaded", () => {
        const controller = new NotesController($);
        const delegate = {
            onLoadNotesDone: (response, hidesPreview) => {
            }
        };

        const data = {'response': true};
        sinon.stub($, 'ajax').yieldsTo("success", data);

        let mock = sinon.mock(delegate);
        let expectation = mock.expects("onLoadNotesDone").withArgs(true, true).exactly(1);
        controller.delegate = delegate;
        controller.load(List.Trash, true);

        assert($.ajax["calledOnce"]);
        expectation.verify();
    });

    it("calls delegate after trashing note", () => {
        const controller = new NotesController($);
        const delegate = {
            onTrashNoteDone: () => {
            }
        };

        sinon.stub($, 'ajax').yieldsTo("success");

        let mock = sinon.mock(delegate);
        let expectation = mock.expects("onTrashNoteDone").exactly(1);
        controller.delegate = delegate;
        controller.trashNote("0A6E19AD-34D7-4C00-875A-40F9E7EADD06");

        assert($.ajax["calledOnce"]);
        expectation.verify();
    });

    it("calls delegate after saving note in non-silent mode", () => {
        const controller = new NotesController($);
        const note = new Note({});
        const delegate = {
            onSaveNoteDone: () => {
            }
        };

        sinon.stub($, 'ajax').yieldsTo("success");

        let mock = sinon.mock(delegate);
        let expectation = mock.expects("onSaveNoteDone").exactly(1);
        controller.delegate = delegate;
        controller.saveNote(note);

        assert($.ajax["calledOnce"]);
        expectation.verify();
    });

    it("does not call delegate after saving note in silent mode", () => {
        const controller = new NotesController($);
        const note = new Note({});
        const delegate = {
            onSaveNoteDone: () => {
            }
        };

        sinon.stub($, 'ajax').yieldsTo("success");

        let mock = sinon.mock(delegate);
        let expectation = mock.expects("onSaveNoteDone").never();
        controller.delegate = delegate;
        controller.saveNote(note, true);

        assert($.ajax["calledOnce"]);
        expectation.verify();
    });

    afterEach(function () {
        // When the test either fails or passes, restore the original
        // jQuery ajax function (Sinon.JS also provides tools to help
        // test frameworks automate clean-up like this)
        const restore = $.ajax["restore"];
        if (restore) {
            restore();
        }
    });
});
