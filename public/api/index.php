<?php
/**
 * Notes Application
 *
 * @author    Adrian Kosmaczewski <learn@akosma.training>
 * @copyright 2017 Adrian Kosmaczewski
 * @license   MIT
 *
 * Copyright © 2017. Adrian Kosmaczewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */

declare(strict_types=1);

/**
 * Notes
 * Created by akosma
 * Date: 25.08.17 09:20
 */

require_once "../../bootstrap.php";

use akosma\notes\controllers\ExceptionController;
use akosma\notes\controllers\NotAllowedHandler;
use akosma\notes\controllers\NotFoundController;
use akosma\notes\controllers\PhpErrorHandler;
use akosma\notes\helpers\Frontend;
use akosma\notes\middleware\Authentication;
use akosma\notes\middleware\Serializer;
use Monolog\Handler\ChromePHPHandler;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Slim\App;

$configuration = [
    "settings"          => [
        "displayErrorDetails" => $ini["configuration"]["displayErrorDetails"],
        "email"               => $ini["email"],
        "smtp"                => $ini["smtp"],
        "telegram"            => $ini["telegram"]["secret"]
    ],
    "entityManager"     => $entityManager,
    "errorHandler"      => function ($c) {
        return ExceptionController::createWithContainer($c);
    },
    "notFoundHandler"   => function ($c) {
        return NotFoundController::createWithContainer($c);
    },
    "notAllowedHandler" => function ($c) {
        return NotAllowedHandler::createWithContainer($c);
    },
    "phpErrorHandler"   => function ($c) {
        return PhpErrorHandler::createWithContainer($c);
    },
    "logger"            => function ($c) use ($isDevMode) {
        $logger = new Logger("notes");
        $file_handler = new StreamHandler("../../logs/app.log", Logger::ERROR);
        $logger->pushHandler($file_handler);
        if ($isDevMode) {
            $chrome_handler = new ChromePHPHandler(Logger::API);
            $logger->pushHandler($chrome_handler);
        }

        return $logger;
    },
];

$container = new \Slim\Container($configuration);
$app = new App($container);

$username = $ini["security"]["username"];
$password = $ini["security"]["password"];
$auth = new Authentication($container, $username, $password);
$serializer = new Serializer($container);

// Public routes, without auth
$app->get("/ping", Frontend::class . ":ping");
$app->post("/telegram/{secret}", Frontend::class . ":telegram");

// Private routes, behind HTTP Basic Auth
$app->group('/v1', function () {
    $this->get("/notes", Frontend::class . ":getAllNotes");
    $this->post("/notes", Frontend::class . ":createNote");
    $this->get("/notes/{id}", Frontend::class . ":getNote");
    $this->put("/notes/{id}", Frontend::class . ":updateNote");
    $this->put("/notes/{id}/mail", Frontend::class . ":mailNote");
    $this->put("/notes/{id}/trash", Frontend::class . ":trashNote");
    $this->put("/notes/{id}/restore", Frontend::class . ":untrashNote");
    $this->put("/notes/{id}/pin", Frontend::class . ":pinNote");
    $this->put("/notes/{id}/unpin", Frontend::class . ":unpinNote");
    $this->put("/notes/{id}/publish", Frontend::class . ":publishNote");
    $this->put("/notes/{id}/unpublish", Frontend::class . ":unpublishNote");
    $this->get("/search/{text}", Frontend::class . ":search");
    $this->get("/backup", Frontend::class . ":backup");
    $this->get("/tags", Frontend::class . ":getAllTags");
    $this->get("/tags/{name}", Frontend::class . ":getAllNotesForTag");
    $this->get("/trash", Frontend::class . ":getTrash");
    $this->delete("/trash", Frontend::class . ":emptyTrash");
})->add($auth);

$app->add($serializer);

$app->run();
