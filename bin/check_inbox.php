#!/usr/local/php71/bin/php
<?php
/**
 * Notes Application
 *
 * @author    Adrian Kosmaczewski <learn@akosma.training>
 * @copyright 2017 Adrian Kosmaczewski
 * @license   MIT
 *
 * Copyright © 2017. Adrian Kosmaczewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */

declare(strict_types=1);

use akosma\notes\controllers\NoteController;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

/**
 * Notes – check_inbox.php
 * Created by akosma
 * Date: 11.09.17 15:24
 */

require_once dirname(__FILE__) . "/../bootstrap.php";

function importNote($headers, $body, Logger $logger, NoteController $controller): void {
    $subject = $headers->subject;
    $from = $headers->from[0];
    $sender = $from->personal;
    $fromEmail = $from->mailbox . "@" . $from->host;
    $date = $headers->date;
    $destination = $headers->toaddress;

    $str = <<< EOL
# $subject

    -----Original Message-----
    From: $sender [mailto:$fromEmail] 
    Sent: $date
    To: $destination <$destination>
    Subject: $subject

$body
EOL;

    $logger->addInfo("Creating note '$subject' from email by $fromEmail");
    $contents = $str;
    $tags = ['email'];
    $controller->createNote($contents, $tags);
}

function createLogger(): \Monolog\Logger {
    $logger = new Logger("check_inbox");
    $file_handler =
        new StreamHandler(dirname(__FILE__) . "/../logs/app.log", Logger::INFO);
    $logger->pushHandler($file_handler);

    return $logger;
}

function openInbox($ini) {
    $emailProtocol = $ini["configuration"]["emailProtocol"];
    $config = $ini[ $emailProtocol ];

    $host = $config["host"];
    $port = $config["port"];
    $username = $config["username"];
    $password = $config["password"];

    $mailbox = "{" . $host . ":" . $port . "/imap}";
    $inbox = imap_open($mailbox, $username, $password);

    return $inbox;
}

function createNoteController($entityManager, $logger): NoteController {
    $configuration = [
        "entityManager" => $entityManager,
        "logger"        => $logger,
    ];

    $container = new \Slim\Container($configuration);
    $controller = NoteController::createWithContainer($container);

    return $controller;
}

$inbox = openInbox($ini);
$messageCount = imap_num_msg($inbox);

$logger = createLogger();
$logger->addInfo("Messages available: $messageCount");

$controller = createNoteController($entityManager, $logger);

for ($messageID = 1; $messageID <= $messageCount; $messageID++) {
    $headers = imap_headerinfo($inbox, $messageID);
    $body = imap_fetchbody($inbox, $messageID, "1");
    importNote($headers, $body, $logger, $controller);
    imap_delete($inbox, $messageID);
}

imap_expunge($inbox);
imap_close($inbox);
