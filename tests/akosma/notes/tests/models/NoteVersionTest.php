<?php
/**
 * Notes Application
 *
 * @author Adrian Kosmaczewski <learn@akosma.training>
 * @copyright 2017 Adrian Kosmaczewski
 * @license MIT
 *
 * Copyright © 2017. Adrian Kosmaczewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */

declare(strict_types=1);

/**
 * Notes
 * Created by akosma
 * Date: 24.08.17 14:20
 */

namespace akosma\notes\tests\models;

use akosma\notes\models\Note;
use akosma\notes\models\NoteVersion;
use PHPUnit\Framework\TestCase;

class NoteVersionTest extends TestCase {
    /**
     * @test
     */
    public function versionHasSameContents() {
        $contents =
            "Title\n\nThese are the contents of the note\n\nMore contents";
        $note = Note::create();
        $note->setContents($contents);
        $version = NoteVersion::createFromNote($note);
        $this->assertEquals($contents, $version->getContents());
    }

    /**
     * @test
     */
    public function originalNoteHasVersion() {
        $note = Note::create();
        $version = NoteVersion::createFromNote($note);
        $this->assertEquals($note, $version->getNote());

        $versions = $note->getVersions();
        $this->assertEquals($version, $versions[0]);
    }

    /**
     * @test
     */
    public function originalHasSameIdAsVersion() {
        $note = Note::create();
        $version = NoteVersion::createFromNote($note);
        $this->assertEquals($version->getNoteId(), $note->getId());
    }

    /**
     * @test
     */
    public function originalHasSameCreationDateAsVersion() {
        $note = Note::create();
        $version = NoteVersion::createFromNote($note);
        $this->assertEquals($version->getCreationDate()->getTimestamp(),
            $note->getCreationDate()->getTimestamp());
    }

    /**
     * @test
     */
    public function canGetVersionId() {
        $note = Note::create();
        $version = NoteVersion::createFromNote($note);
        $this->assertEquals(-1, $version->getId());
    }
}
