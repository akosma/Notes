<?php
/**
 * Notes Application
 *
 * @author Adrian Kosmaczewski <learn@akosma.training>
 * @copyright 2017 Adrian Kosmaczewski
 * @license MIT
 *
 * Copyright © 2017. Adrian Kosmaczewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */

declare(strict_types=1);

/**
 * Notes
 * Created by akosma
 * Date: 01.09.17 09:59
 */

namespace akosma\notes\tests\models;

use akosma\notes\models\Note;
use akosma\notes\models\Tag;
use PHPUnit\Framework\TestCase;

/**
 * Class TagTest
 *
 * @package akosma\notes\tests
 * @coversDefaultClass \akosma\notes\models\Tag
 */
class TagTest extends TestCase {
    /**
     * @test
     * @covers ::getName
     */
    public function canCreateWithName() {
        $tag = Tag::createWithName("Test");
        $this->assertEquals("Test", $tag->getName());
    }

    /**
     * @test
     * @covers ::addNote
     * @covers ::removeNote
     * @covers ::getNotes
     */
    public function canHaveNotesAssociated() {
        $note = Note::createWithContents("Test");
        $tag = Tag::createWithName("tag");

        $note->setTags([$tag]);
        $this->assertContains($note, $tag->getNotes());
        $this->assertContains($tag, $note->getTags());
        $note->setTags([]);
        $this->assertNotContains($note, $tag->getNotes());
        $this->assertNotContains($tag, $note->getTags());
    }

    /**
     * @test
     * @covers ::jsonSerialize
     * @covers ::createWithName
     * @covers ::__construct
     */
    public function canSerializeToJson() {
        $tagName = "news";
        $tag = Tag::createWithName($tagName);
        $json = $tag->jsonSerialize();
        $this->assertEquals(-1, $json["id"]);
        $this->assertEquals($tagName, $json["name"]);
    }

    /**
     * @test
     * @covers ::getId
     */
    public function byDefaultIdIsMinusOne() {
        $tagName = "news";
        $tag = Tag::createWithName($tagName);
        $this->assertEquals(-1, $tag->getId());
    }
}
