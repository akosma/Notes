<?php
/**
 * Notes Application
 *
 * @author Adrian Kosmaczewski <learn@akosma.training>
 * @copyright 2017 Adrian Kosmaczewski
 * @license MIT
 *
 * Copyright © 2017. Adrian Kosmaczewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */

declare(strict_types=1);

/**
 * User: akosma
 * Date: 24.08.17 13:31
 */

namespace akosma\notes\tests\models;

use akosma\notes\helpers\StringHelper;
use akosma\notes\models\Note;
use akosma\notes\models\Tag;
use PHPUnit\Framework\TestCase;

/**
 * Class NoteTest
 *
 * @package akosma\notes\tests
 * @coversDefaultClass \akosma\notes\models\Note
 */
class NoteTest extends TestCase {
    /**
     * @test
     * @covers ::createWithContents
     */
    public function noteCanBeCreatedWithContents() {
        $contents =
            "Title\n\nThese are the contents of the note\n\nMore contents";
        $note = Note::createWithContents($contents);
        $this->assertEquals($contents, $note->getContents());
    }

    /**
     * @test
     * @covers ::createWithContents
     * @covers ::getTags
     * @covers ::setTags
     */
    public function noteCanBeCreatedWithTags() {
        $contents =
            "Title\n\nThese are the contents of the note\n\nMore contents";
        $tags = $this->createSampleTags();
        $note = Note::createWithContents($contents, $tags);
        $this->assertEquals($tags, $note->getTags());
    }

    /**
     * @test
     * @covers ::createWithContents
     * @covers ::setId
     * @covers ::getId
     */
    public function noteCanBeCreatedWithId() {
        $tags = $this->createSampleTags();
        $uuid = "9A576C68-6012-4905-978F-68AD3253AFC1";
        $note = Note::createWithContents("Test", $tags, $uuid);
        $this->assertEquals("Test", $note->getContents());
        $this->assertEquals($tags, $note->getTags());
        $this->assertEquals($uuid, $note->getId());
    }

    /**
     * @test
     * @covers ::getTitle
     */
    public function titleOfNoteIsAlwaysFirstLineOfContents() {
        $contents =
            "Title\n\nThese are the contents of the note\n\nMore contents";
        $note = Note::createWithContents($contents);
        $this->assertEquals("Title", $note->getTitle());
    }

    /**
     * @test
     * @covers ::getTitle
     */
    public function titleOfEmptyContentsIsEmptyString() {
        $contents = "";
        $note = Note::createWithContents($contents);
        $this->assertEquals("", $note->getTitle());
    }

    /**
     * @test
     * @covers ::getCreationDate
     * @covers ::getLastModificationDate
     */
    public function creationDateOfNoteIsTheSameAsModificationOnCreation() {
        $note = Note::create();
        $this->assertNotNull($note->getCreationDate());
        $this->assertEquals($note->getLastModificationDate()->getTimestamp(),
            $note->getCreationDate()->getTimestamp());
    }

    /**
     * @test
     * @covers ::setTags
     * @covers ::getLastModificationDate
     */
    public function tagsCanBeUpdated() {
        $contents =
            "Title\n\nThese are the contents of the note\n\nMore contents";
        $tags = $this->createSampleTags();
        $note = Note::createWithContents($contents, $tags);
        $firstDate = $note->getLastModificationDate()->getTimestamp();
        $this->assertEquals($tags, $note->getTags());
        $newTags = [
            Tag::createWithName("three"), Tag::createWithName("two"),
            Tag::createWithName("four"),
        ];
        $note->setTags($newTags);
        $this->assertEquals($newTags, $note->getTags());
        $secondDate = $note->getLastModificationDate()->getTimestamp();
        $this->assertGreaterThanOrEqual($firstDate, $secondDate);
    }

    /**
     * @test
     * @covers ::isPinned
     */
    public function noteIsNotPinnedByDefault() {
        $note = Note::create();
        $this->assertFalse($note->isPinned());
    }

    /**
     * @test
     * @covers ::isDeleted
     */
    public function noteIsNotDeletedByDefault() {
        $note = Note::create();
        $this->assertFalse($note->isDeleted());
    }

    /**
     * @test
     * @covers ::isDeleted
     * @covers ::setDeleted
     */
    public function noteCanBeDeleted() {
        $note = Note::create();
        $note->setDeleted(true);
        $this->assertTrue($note->isDeleted());
        $note->setDeleted(false);
        $this->assertFalse($note->isDeleted());
    }

    /**
     * @test
     * @covers ::getId
     */
    public function noteHasAutogeneratedId() {
        $note = Note::create();
        $this->assertNotNull($note->getId());
    }

    /**
     * @test
     * @covers ::isPinned
     * @covers ::setPinned
     */
    public function noteCanBePinned() {
        $note = Note::create();
        $note->setPinned(true);
        $this->assertTrue($note->isPinned());
        $note->setPinned(false);
        $this->assertFalse($note->isPinned());
    }

    /**
     * @test
     * @covers ::jsonSerialize
     */
    public function noteCanBeSerializedIntoJson() {
        $tags = $this->createSampleTags();
        $uuid = "9A576C68-6012-4905-978F-68AD3253AFC1";
        $note = Note::createWithContents("Test", $tags, $uuid);
        $json = $note->jsonSerialize();
        $this->assertEquals($json["contents"], "Test");
        $this->assertEquals(["one", "two", "three"], $json["tags"]);
        $this->assertEquals($uuid, $json["id"]);
    }

    /**
     * @test
     * @covers ::setSlug
     * @covers ::getSlug
     * @covers ::isPublished
     */
    public function noteCanHaveSlugAndBePublished() {
        $note = Note::createWithContents("Test");
        $slug = StringHelper::generateRandomString();
        $this->assertFalse($note->isPublished());
        $note->setSlug($slug);
        $this->assertTrue($note->isPublished());
        $this->assertEquals($slug, $note->getSlug());
        $note->setSlug();
        $this->assertFalse($note->isPublished());
    }

    /**
     * @return array
     */
    private function createSampleTags(): array {
        $tags = [
            Tag::createWithName("one"), Tag::createWithName("two"),
            Tag::createWithName("three"),
        ];

        return $tags;
    }
}
