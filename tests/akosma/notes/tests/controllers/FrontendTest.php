<?php
/**
 * Notes Application
 *
 * @author    Adrian Kosmaczewski <learn@akosma.training>
 * @copyright 2017 Adrian Kosmaczewski
 * @license   MIT
 *
 * Copyright © 2017. Adrian Kosmaczewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */

declare(strict_types=1);

/**
 * Notes
 * Created by akosma
 * Date: 29.08.17 21:19
 */

namespace akosma\notes\tests\controllers;

use akosma\notes\exceptions\NoteNotFoundException;
use akosma\notes\helpers\Frontend;
use akosma\notes\models\Note;
use Slim\Http\Environment;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Class FrontendTest
 *
 * @package akosma\notes\tests
 * @coversDefaultClass \akosma\notes\helpers\Frontend
 */
class FrontendTest extends BaseControllerTest {
    /**
     * @var Request
     */
    private $request = null;
    /**
     * @var Response
     */
    private $response = null;
    /**
     * @var Frontend
     */
    private $frontend = null;

    public function setUp() {
        parent::setUp();

        $env = Environment::mock();
        $request = Request::createFromEnvironment($env);
        $this->request =
            $request->withHeader("Content-Type", "application/json");
        $this->response = new Response();
        $this->frontend = new Frontend($this->getContainer());
    }

    /**
     * @test
     * @covers ::ping
     * @covers ::__construct
     */
    public function ping() {
        $resp = $this->frontend->ping($this->request, $this->response);
        $this->assertTrue($resp->isOk());
    }

    /**
     * @test
     * @covers ::getAllNotes
     */
    public function canGetAllNotes() {
        $resp = $this->frontend->getAllNotes($this->request, $this->response);
        $this->assertTrue($resp->isOk());
    }

    /**
     * @test
     * @covers ::getAllNotesForTag
     */
    public function canGetAllNotesForTag() {
        $request = $this->request->withAttribute("name", "test");
        $resp = $this->frontend->getAllNotesForTag($request, $this->response);
        $this->assertTrue($resp->isOk());
    }

    /**
     * @test
     * @covers ::getAllTags
     */
    public function canGetAllTags() {
        $resp = $this->frontend->getAllTags($this->request, $this->response);
        $this->assertTrue($resp->isOk());
    }

    /**
     * @test
     * @covers ::getNote
     * @covers ::getLogger
     * @covers ::getContainer
     */
    public function canGetNote() {
        $uuid = $this->createSampleNote();

        $request = $this->request->withAttribute("id", $uuid);
        $resp = $this->frontend->getNote($request, $this->response);
        $this->assertTrue($resp->isOk());
    }

    /**
     * @test
     * @covers ::getNote
     * @covers \akosma\notes\exceptions\NoteNotFoundException::__construct
     * @covers \akosma\notes\exceptions\NoteNotFoundException::getStatusCode
     * @covers \akosma\notes\exceptions\NoteNotFoundException::getId
     */
    public function cannotGetNonExistentNote() {
        $uuid = "B1C8234D-4B03-4FDE-AD7B-C21BAEF9547B";

        $request = $this->request->withAttribute("id", $uuid);

        try {
            $this->frontend->getNote($request, $this->response);
            $this->assertTrue(false);
        }
        catch (NoteNotFoundException $exception) {
            $this->assertEquals(404, $exception->getStatusCode());
            $this->assertEquals($uuid, $exception->getId());
        }
    }

    /**
     * @test
     * @covers ::createNote
     * @covers ::writeResponse
     */
    public function canCreateNoteOnlyWithContents() {
        $data = [
            "contents" => "New note",
        ];
        $request = $this->request->withParsedBody($data);
        $resp = $this->frontend->createNote($request, $this->response);
        $this->assertTrue($resp->isSuccessful());
    }

    /**
     * @test
     * @covers ::createNote
     */
    public function canCreateNoteWithContentsAndTags() {
        $data = [
            "contents" => "New note",
            "tags"     => ["this", "that"],
        ];
        $request = $this->request->withParsedBody($data);
        $resp = $this->frontend->createNote($request, $this->response);
        $this->assertTrue($resp->isSuccessful());
    }

    /**
     * @test
     * @covers ::createNote
     */
    public function canCreateNoteWithContentsAndTagsAndId() {
        $data = [
            "contents" => "New note",
            "tags"     => ["this", "that"],
            "id"       => "87C3638B-DEA1-4037-97D0-6C3E391D5B17",
        ];
        $request = $this->request->withParsedBody($data);
        $resp = $this->frontend->createNote($request, $this->response);
        $this->assertTrue($resp->isSuccessful());
    }

    /**
     * @test
     * @covers ::updateNote
     */
    public function canUpdateNote() {
        $uuid = $this->createSampleNote();
        $data = [
            "contents" => "Updated note",
            "tags"     => ["this", "that"],
            "id"       => "87C3638B-DEA1-4037-97D0-6C3E391D5B17",
        ];
        $request = $this->request->withAttribute("id", $uuid);
        $request = $request->withParsedBody($data);
        $resp = $this->frontend->updateNote($request, $this->response);
        $this->assertTrue($resp->isSuccessful());
    }

    /**
     * @test
     * @covers ::trashNote
     */
    public function canTrashNote() {
        $uuid = $this->createSampleNote();
        $request = $this->request->withAttribute("id", $uuid);
        $resp = $this->frontend->trashNote($request, $this->response);
        $this->assertTrue($resp->isSuccessful());
    }

    /**
     * @test
     * @covers ::getTrash
     */
    public function canGetTrash() {
        $resp = $this->frontend->getTrash($this->request, $this->response);
        $this->assertTrue($resp->isOk());
    }

    private function createSampleNote(): string {
        $uuid = "5964FD90-A74A-4E7B-A58D-3B3E84F88F07";
        $note = Note::createWithContents("Test", null, $uuid);
        $note->setContents("test");
        $this->getEntityManager()->persist($note);
        $this->getEntityManager()->flush();

        return $uuid;
    }
}
