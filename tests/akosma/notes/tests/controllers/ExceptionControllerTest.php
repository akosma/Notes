<?php
/**
 * Notes Application
 *
 * @author Adrian Kosmaczewski <learn@akosma.training>
 * @copyright 2017 Adrian Kosmaczewski
 * @license MIT
 *
 * Copyright © 2017. Adrian Kosmaczewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */

declare(strict_types=1);

/**
 * Notes
 * Created by akosma
 * Date: 02.09.17 19:23
 */

namespace akosma\notes\tests\controllers;

use akosma\notes\controllers\ExceptionController;
use akosma\notes\exceptions\ForbiddenAccessException;
use akosma\notes\exceptions\NoteNotFoundException;
use Slim\Http\Environment;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Class ExceptionControllerTest
 *
 * @package akosma\notes\tests\controllers
 * @coversDefaultClass \akosma\notes\tests\controllers\ExceptionController
 */
class ExceptionControllerTest extends BaseControllerTest {
    /**
     * @test
     */
    public function canHandleForbiddenAccessExceptions() {
        $env = Environment::mock();
        $request = Request::createFromEnvironment($env);
        $response = new Response();
        $exceptionController =
            ExceptionController::createWithContainer($this->getContainer());
        $response = $exceptionController($request, $response,
            new ForbiddenAccessException());
        $this->assertEquals(401, $response->getStatusCode());
    }

    /**
     * @test
     */
    public function canHandleBaseExceptions() {
        $env = Environment::mock();
        $request = Request::createFromEnvironment($env);
        $response = new Response();
        $exceptionController =
            ExceptionController::createWithContainer($this->getContainer());
        $uuid = "554D1771-3D21-48C5-A3E2-CF0B36215D4A";
        $exception = new NoteNotFoundException($uuid);
        $response = $exceptionController($request, $response,
            $exception);
        $this->assertEquals($exception->getStatusCode(),
            $response->getStatusCode());
    }

    /**
     * @test
     */
    public function canHandleGenericExceptions() {
        $env = Environment::mock();
        $request = Request::createFromEnvironment($env);
        $response = new Response();
        $exceptionController =
            ExceptionController::createWithContainer($this->getContainer());
        $exception = new \Exception();
        $response = $exceptionController($request, $response,
            $exception);
        $this->assertEquals(500,
            $response->getStatusCode());
    }
}
