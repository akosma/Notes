<?php
/**
 * Notes Application
 *
 * @author Adrian Kosmaczewski <learn@akosma.training>
 * @copyright 2017 Adrian Kosmaczewski
 * @license MIT
 *
 * Copyright © 2017. Adrian Kosmaczewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */

declare(strict_types=1);

/**
 * Notes
 * Created by akosma
 * Date: 25.08.17 09:35
 */

namespace akosma\notes\tests\controllers;

use akosma\notes\controllers\NoteController;
use akosma\notes\controllers\TagController;
use PHPUnit\Framework\TestCase;

/**
 * Class TagControllerTest
 *
 * @package akosma\notes\tests
 * @coversDefaultClass \akosma\notes\controllers\TagController
 */
class TagControllerTest extends BaseControllerTest {
    /**
     * @test
     * @covers ::createWithContainer
     * @covers ::getOrCreateTags
     * @covers ::getTagRepository
     */
    public function tagsCanBeCreated() {
        $name = "Test";
        $controller = TagController::createWithContainer($this->getContainer());
        $tag = $controller->getOrCreateTags([$name])[0];
        $this->assertEquals($name, $tag->getName());
        $this->assertEquals(1, $tag->getId());
    }

    /**
     * @test
     * @covers ::createWithContainer
     * @covers ::getOrCreateTags
     */
    public function tagsAreNeverDuplicated() {
        $tagNames = ["one", "two", "two", "three", "four", "four"];
        $controller = TagController::createWithContainer($this->getContainer());
        $tags = $controller->getOrCreateTags($tagNames);
        $this->assertCount(4, $tags);
    }

    /**
     * @test
     * @covers ::createWithContainer
     * @covers ::getOrCreateTags
     */
    public function tagsAreNeverEmptyStringsOrHaveExtraSpaces() {
        $tagNames = ["one", "  two ", "      two", "", "", "four  "];
        $controller = TagController::createWithContainer($this->getContainer());
        $tags = $controller->getOrCreateTags($tagNames);
        $this->assertCount(3, $tags);
    }

    /**
     * @test
     * @covers ::getAllTags
     */
    public function canRetrieveAllTags() {
        $tagNames = ["one", "two", "two", "three", "four", "four"];
        $controller = TagController::createWithContainer($this->getContainer());
        $controller->getOrCreateTags($tagNames);
        $tags = $controller->getAllTags();
        $this->assertCount(4, $tags);
    }

    /**
     * @test
     * @covers ::getAllNotesByTag
     */
    public function canRetrieveNonDeletedNotesAssociatedToTags() {
        $this->createSampleNotes();

        $tag = "news";
        $tagController = TagController::createWithContainer($this->getContainer());
        $notes = $tagController->getAllNotesByTag($tag);
        $this->assertCount(3, $notes);
    }

    /**
     * @test
     * @covers ::getAllNotesByTag
     */
    public function emptyArrayReturnedForNonExistentTag() {
        $this->createSampleNotes();

        $tag = "whatever";
        $tagController = TagController::createWithContainer($this->getContainer());
        $notes = $tagController->getAllNotesByTag($tag);
        $this->assertCount(0, $notes);
    }

    private function createSampleNotes(): void {
        $noteController =
            NoteController::createWithContainer($this->getContainer());
        $noteController->createNote("First note", ["news", "world", "music"]);
        $noteController->createNote("Second note", ["news", "europe", "art"]);
        $noteController->createNote("Third note", ["art", "music"]);
        $noteController->createNote("Fourth note", ["news", "art", "asia"]);
        $deleted =
            $noteController->createNote("Deleted note", ["world", "news"]);
        $deleted->setDeleted(true);
    }
}
