<?php
/**
 * Notes Application
 *
 * @author Adrian Kosmaczewski <learn@akosma.training>
 * @copyright 2017 Adrian Kosmaczewski
 * @license MIT
 *
 * Copyright © 2017. Adrian Kosmaczewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */

declare(strict_types=1);

/**
 * Notes
 * Created by akosma
 * Date: 24.08.17 15:21
 */

namespace akosma\notes\tests\controllers;

use akosma\notes\controllers\NoteController;

/**
 * Class NoteControllerTest
 *
 * @package akosma\notes\tests
 * @coversDefaultClass \akosma\notes\controllers\NoteController
 */
class NoteControllerTest extends BaseControllerTest {
    /**
     * @test
     * @covers ::createNote
     * @covers ::getEntityManager
     */
    public function notesCanBeCreated() {
        $controller =
            NoteController::createWithContainer($this->getContainer());
        $contents = "Test Contents\n\nThis is a test";
        $note = $controller->createNote($contents);
        $this->assertEquals("Test Contents", $note->getTitle());
        $this->assertEquals($contents, $note->getContents());
    }

    /**
     * @test
     * @covers ::createNote
     */
    public function notesCanBeCreatedWithTags() {
        $controller =
            NoteController::createWithContainer($this->getContainer());
        $contents = "Test Contents\n\nThis is a test";
        $tagNames = ["one", "two", "three"];
        $note = $controller->createNote($contents, $tagNames);
        $this->assertEquals("Test Contents", $note->getTitle());
        $this->assertEquals($contents, $note->getContents());
        $this->assertCount(3, $note->getTags());
    }

    /**
     * @test
     * @covers ::createNote
     */
    public function notesCanBeCreatedWithId() {
        $controller =
            NoteController::createWithContainer($this->getContainer());
        $contents = "Test Contents\n\nThis is a test";
        $tagNames = ["one", "two", "three"];
        $id = "65C31B3F-326D-449C-BBE4-D65E3027E584";
        $note = $controller->createNote($contents, $tagNames, $id);
        $this->assertEquals("Test Contents", $note->getTitle());
        $this->assertEquals($id, $note->getId());
        $this->assertEquals($contents, $note->getContents());
        $this->assertCount(3, $note->getTags());
    }

    /**
     * @test
     * @covers ::getNote
     * @covers ::getNoteRepository
     */
    public function notesCanBeRetrieved() {
        $controller =
            NoteController::createWithContainer($this->getContainer());
        $contents = "Test Contents\n\nThis is a test";
        $tagNames = ["one", "two", "three"];
        $note = $controller->createNote($contents, $tagNames);
        $id = $note->getId();

        $found = $controller->getNote($id);
        $this->assertEquals($note->getId(), $found->getId());
    }

    /**
     * @test
     * @covers ::getNote
     * @expectedException \akosma\notes\exceptions\NoteNotFoundException
     * @expectedExceptionCode 404
     */
    public function nonExistentNotesCannotBeFound() {
        $controller =
            NoteController::createWithContainer($this->getContainer());
        $fakeId = "87683C16-494A-49BA-8FA8-F805B762F3BD";
        $controller->getNote($fakeId);
    }

    /**
     * @test
     * @covers ::getAllNotes
     */
    public function canRetrieveAllNotes() {
        $controller =
            NoteController::createWithContainer($this->getContainer());
        $controller->createNote("First note", ["news", "world", "music"]);
        $controller->createNote("Second note", ["news", "europe", "art"]);
        $controller->createNote("Third note", ["art", "music"]);
        $controller->createNote("Fourth note", ["news", "art", "asia"]);

        $notes = $controller->getAllNotes();
        $this->assertCount(4, $notes);
    }

    /**
     * @test
     * @covers ::getAllNotes
     */
    public function pinnedNotesAreAtTheTop() {
        $controller =
            NoteController::createWithContainer($this->getContainer());
        $controller->createNote("First note", ["news", "world", "music"]);
        $controller->createNote("Second note", ["news", "europe", "art"]);
        $note = $controller->createNote("Third note", ["art", "music"]);
        $controller->createNote("Fourth note", ["news", "art", "asia"]);
        $controller->pinNote($note->getId());

        $notes = $controller->getAllNotes();
        /** @var \akosma\notes\models\Note $pinnedNote */
        $pinnedNote = $notes[0];
        $this->assertEquals($pinnedNote->getTitle(), "Third note");
        $this->assertTrue($pinnedNote->isPinned());
    }

    /**
     * @test
     * @covers ::pinNote
     */
    public function canPinNotes() {
        $controller =
            NoteController::createWithContainer($this->getContainer());
        $note =
            $controller->createNote("First note", ["news", "world", "music"]);
        $id = $note->getId();
        $controller->pinNote($id);
        $note = $controller->getNote($id);
        $this->assertTrue($note->isPinned());
    }

    /**
     * @test
     * @covers ::unpinNote
     */
    public function canUnPinNotes() {
        $controller =
            NoteController::createWithContainer($this->getContainer());
        $note =
            $controller->createNote("First note", ["news", "world", "music"]);
        $id = $note->getId();
        $controller->pinNote($id);
        $note = $controller->getNote($id);
        $this->assertTrue($note->isPinned());
        $controller->unpinNote($id);
        $note = $controller->getNote($id);
        $this->assertFalse($note->isPinned());
    }

    /**
     * @test
     * @covers ::trashNote
     * @covers ::untrashNote
     * @covers ::getTrash
     * @covers ::emptyTrash
     */
    public function notesCanBeSentToTrash() {
        $controller =
            NoteController::createWithContainer($this->getContainer());
        $controller->createNote("First note", ["news", "world", "music"]);
        $note2 =
            $controller->createNote("Second note", ["news", "europe", "art"]);
        $controller->createNote("Third note", ["art", "music"]);
        $controller->createNote("Fourth note", ["news", "art", "asia"]);

        $notes = $controller->getAllNotes();
        $this->assertCount(4, $notes);
        $trash = $controller->getTrash();
        $this->assertCount(0, $trash);

        $id2 = $note2->getId();
        $controller->trashNote($id2);
        $notes = $controller->getAllNotes();
        $this->assertCount(3, $notes);
        $trash = $controller->getTrash();
        $this->assertCount(1, $trash);

        $controller->untrashNote($id2);
        $notes = $controller->getAllNotes();
        $this->assertCount(4, $notes);
        $trash = $controller->getTrash();
        $this->assertCount(0, $trash);

        $controller->trashNote($id2);
        $controller->emptyTrash();
        $notes = $controller->getAllNotes();
        $this->assertCount(3, $notes);
        $trash = $controller->getTrash();
        $this->assertCount(0, $trash);
    }

    /**
     * @test
     * @covers ::updateNote
     * @covers ::getVersionsForNote
     * @covers ::restoreNoteToVersion
     */
    public function notesCanBeUpdatedAndVersionsRetrieved() {
        $controller =
            NoteController::createWithContainer($this->getContainer());
        $note =
            $controller->createNote("First note", ["news", "world", "music"]);
        $id = $note->getId();
        $firstDate = $note->getLastModificationDate()->getTimestamp();

        $controller->updateNote($id, "Updated",
            ["toto", "toti", "music", "world", "yeah"]);
        $found = $controller->getNote($id);
        $this->assertEquals("Updated", $found->getContents());

        $secondDate = $note->getLastModificationDate()->getTimestamp();
        $this->assertGreaterThanOrEqual($firstDate, $secondDate);

        $this->assertCount(5, $found->getTags());

        $versions = $controller->getVersionsForNote($id);
        $this->assertCount(1, $versions);
        /** @var \akosma\notes\models\NoteVersion $oldVersion */
        $oldVersion = $versions[0];
        $oldVersionId = $oldVersion->getId();

        $controller->restoreNoteToVersion($id, $oldVersionId);
        $versions = $controller->getVersionsForNote($id);
        $this->assertCount(2, $versions);

        $found = $controller->getNote($id);
        $this->assertEquals("First note", $found->getContents());

        // Tag count does not change
        $this->assertCount(5, $found->getTags());
    }

    /**
     * @test
     * @covers ::getVersionsForNote
     * @expectedException \akosma\notes\exceptions\NoteNotFoundException
     * @expectedExceptionCode 404
     */
    public function cannotRetrieveVersionsFromNonExistentNotes() {
        $controller =
            NoteController::createWithContainer($this->getContainer());
        $fakeId = "3B8A35B6-B828-4A4D-80CE-02EE02337C49";
        $controller->getVersionsForNote($fakeId);
    }

    /**
     * @test
     * @covers ::restoreNoteToVersion
     * @expectedException \akosma\notes\exceptions\VersionNotFoundException
     * @expectedExceptionCode 404
     */
    public function cannotRetrieveNonExistentVersionFromNote() {
        $controller =
            NoteController::createWithContainer($this->getContainer());
        $note =
            $controller->createNote("First note", ["news", "world", "music"]);
        $id = $note->getId();

        $controller->updateNote($id, "Updated",
            ["toto", "toti", "music", "world", "yeah"]);
        $found = $controller->getNote($id);
        $this->assertEquals("Updated", $found->getContents());

        $controller->restoreNoteToVersion($id, 654);
    }

    /**
     * @test
     * @covers ::publishNote
     * @covers ::unpublishNote
     */
    public function canPublishNote() {
        $controller =
            NoteController::createWithContainer($this->getContainer());
        $contents = "Test Contents\n\nThis is a test";
        $note = $controller->createNote($contents);
        $this->assertNull($note->getSlug());

        $controller->publishNote($note->getId());
        $this->assertNotNull($note->getSlug());

        $controller->unpublishNote($note->getId());
        $this->assertNull($note->getSlug());
    }
}
