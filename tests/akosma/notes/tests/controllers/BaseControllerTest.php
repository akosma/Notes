<?php
/**
 * Notes Application
 *
 * @author Adrian Kosmaczewski <learn@akosma.training>
 * @copyright 2017 Adrian Kosmaczewski
 * @license MIT
 *
 * Copyright © 2017. Adrian Kosmaczewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */

declare(strict_types=1);

/**
 * Notes
 * Created by akosma
 * Date: 25.08.17 09:35
 */

namespace akosma\notes\tests\controllers;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\SchemaTool;
use Doctrine\ORM\Tools\Setup;
use Monolog\Handler\TestHandler;
use Monolog\Logger;
use PHPUnit\Framework\TestCase;
use Slim\Container;

/**
 * Class BaseControllerTest
 *
 * This is the base class for all test suites that require access
 * to the database. The `setUp()` method creates an in-memory SQLite
 * database for this purpose.
 *
 * @package akosma\notes\tests
 */
abstract class BaseControllerTest extends TestCase {
    /**
     * @var Container
     */
    private $container = null;
    /**
     * @var EntityManager
     */
    private $entityManager = null;

    /**
     * @return \Slim\Container
     */
    protected function getContainer(): \Slim\Container {
        return $this->container;
    }

    /**
     * @return \Doctrine\ORM\EntityManager
     */
    protected function getEntityManager(): \Doctrine\ORM\EntityManager {
        return $this->entityManager;
    }

    /**
     * Called before every test.
     *
     * Sets up an in-memory SQLite database so that all
     * database operations happen in memory, seamlessly to the
     * rest of the application.
     */
    public function setUp() {
        // Build a temporary entity manager that uses SQLite in memory
        $isDevMode = true;
        $config = Setup::createAnnotationMetadataConfiguration([
            __DIR__ . "/../../../../../src/akosma/notes/models",
        ], $isDevMode);
        $conn = ['driver' => 'pdo_sqlite', 'memory' => true];
        $this->entityManager = EntityManager::create($conn, $config);

        // Create the schema
        $schemaTool = new SchemaTool($this->entityManager);
        $mf = $this->entityManager->getMetadataFactory();
        $classes = $mf->getAllMetadata();
        $schemaTool->dropDatabase();
        $schemaTool->createSchema($classes);

        // Logger for testing
        $logger = new Logger('null_logger');
        $handler = new TestHandler();
        $logger->pushHandler($handler);

        // Here we mock the steps done by the Slim framework to route the
        // request to the handler; for that we need a configuration
        // and a mock environment object.
        $conf = [
            "entityManager" => $this->entityManager,
            "logger"        => $logger,
            "template"      => dirname(__FILE__) . "/../../../../../public/pub/template.php"
        ];
        $this->container = new Container($conf);
    }
}
