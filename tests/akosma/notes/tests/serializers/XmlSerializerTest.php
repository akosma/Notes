<?php
/**
 * Notes Application
 *
 * @author Adrian Kosmaczewski <learn@akosma.training>
 * @copyright 2017 Adrian Kosmaczewski
 * @license MIT
 *
 * Copyright © 2017. Adrian Kosmaczewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */

declare(strict_types=1);

/**
 * Notes
 * Created by akosma
 * Date: 02.09.17 12:59
 */

namespace akosma\notes\tests\serializers;

use akosma\notes\helpers\serializers\XmlSerializer;
use PHPUnit\Framework\TestCase;

/**
 * Class XmlSerializerTest
 *
 * @package akosma\notes\tests\serializers
 * @coversDefaultClass \akosma\notes\helpers\serializers\XmlSerializer
 */
class XmlSerializerTest extends TestCase {
    /**
     * @test
     * @covers ::create
     * @covers ::serialize
     * @covers ::__construct
     */
    function canSerialize() {
        $obj = [
            "key1" => "value",
            "key2" => 2,
            "key3" => true,
            "key4" => [1, 2, 3]
        ];
        $serializer = XmlSerializer::create();
        $str = $serializer->serialize($obj);
        $json = <<< EOL
<?xml version="1.0" encoding="UTF-8"?>
<message>
  <key1>value</key1>
  <key2>2</key2>
  <key3>1</key3>
  <key4>
    <obj>1</obj>
    <obj>2</obj>
    <obj>3</obj>
  </key4>
</message>
EOL;
;
        $this->assertEquals($json, $str);
    }

    /**
     * @test
     * @covers ::responseMimeType
     */
    function returnsCorrectMimeType() {
        $serializer = XmlSerializer::create();
        $str = $serializer->responseMimeType();
        $this->assertEquals('text/xml;charset=utf-8', $str);
    }
}
