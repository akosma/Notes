<?php
/**
 * Notes Application
 *
 * @author Adrian Kosmaczewski <learn@akosma.training>
 * @copyright 2017 Adrian Kosmaczewski
 * @license MIT
 *
 * Copyright © 2017. Adrian Kosmaczewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */

declare(strict_types=1);

/**
 * Notes
 * Created by akosma
 * Date: 02.09.17 13:03
 */

namespace akosma\notes\tests\serializers;

use akosma\notes\helpers\serializers\BinaryPlistSerializer;
use akosma\notes\helpers\serializers\JsonSerializer;
use akosma\notes\helpers\serializers\SerializerFactory;
use akosma\notes\helpers\serializers\XmlPlistSerializer;
use akosma\notes\helpers\serializers\XmlSerializer;
use PHPUnit\Framework\TestCase;

/**
 * Class SerializerFactoryTest
 *
 * @package akosma\notes\tests\serializers
 * @coversDefaultClass \akosma\notes\helpers\serializers\SerializerFactory
 */
class SerializerFactoryTest extends TestCase {
    /**
     * @test
     * @covers ::createSerializer
     * @covers ::create
     * @covers ::__construct
     */
    function createsJsonSerializerByDefault() {
        $factory = SerializerFactory::create();
        $serializer = $factory->createSerializer(null);
        $this->assertInstanceOf(JsonSerializer::class, $serializer);
    }

    /**
     * @test
     * @covers ::createSerializer
     * @covers ::create
     * @covers ::__construct
     */
    function canCreateXmlSerializer() {
        $factory = SerializerFactory::create();
        $serializer = $factory->createSerializer("text/xml");
        $this->assertInstanceOf(XmlSerializer::class, $serializer);
    }

    /**
     * @test
     * @covers ::createSerializer
     * @covers ::create
     * @covers ::__construct
     */
    function canCreateXmlPlistSerializer() {
        $factory = SerializerFactory::create();
        $serializer = $factory->createSerializer("application/xml");
        $this->assertInstanceOf(XmlPlistSerializer::class, $serializer);
    }

    /**
     * @test
     * @covers ::createSerializer
     * @covers ::create
     * @covers ::__construct
     */
    function canCreateBinaryPlistSerializer() {
        $factory = SerializerFactory::create();
        $serializer = $factory->createSerializer("application/x-plist");
        $this->assertInstanceOf(BinaryPlistSerializer::class, $serializer);
    }
}
