<?php
/**
 * Notes Application
 *
 * @author    Adrian Kosmaczewski <learn@akosma.training>
 * @copyright 2017 Adrian Kosmaczewski
 * @license   MIT
 *
 * Copyright © 2017. Adrian Kosmaczewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */

declare(strict_types=1);

/**
 * Notes – PublicWebsiteTest.php
 * Created by akosma
 * Date: 11.09.17 14:36
 */

namespace akosma\notes\tests\helpers;

use akosma\notes\helpers\Frontend;
use akosma\notes\helpers\PublicWebsite;
use akosma\notes\models\Note;
use akosma\notes\tests\controllers\BaseControllerTest;
use Slim\Http\Environment;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Class PublicWebsiteTest
 * @package akosma\notes\tests\helpers
 * @coversDefaultClass \akosma\notes\helpers\PublicWebsite
 */
class PublicWebsiteTest extends BaseControllerTest {
    /**
     * @test
     * @covers ::getPublishedNote
     * @covers \akosma\notes\helpers\Frontend::publishNote
     * @covers \akosma\notes\helpers\Frontend::unpublishNote
     */
    public function canGetPublishedNote() {
        // Create mock environment and first request and response
        $env = Environment::mock();
        $request1 = Request::createFromEnvironment($env);
        $request1 =
            $request1->withHeader("Content-Type", "application/json");
        $response1 = new Response();
        $frontend = new Frontend($this->getContainer());
        $publicWebsite = new PublicWebsite($this->getContainer());

        // Create a sample note to play with
        $uuid = $this->createSampleNote();

        // Publish that note, passing the ID
        $request1 = $request1->withAttribute("id", $uuid);
        $resp = $frontend->publishNote($request1, $response1);
        $this->assertTrue($resp->isSuccessful());

        // Get the slug of the note now that it is published
        $obj = json_decode((string)$resp->getBody(), true);
        $slug = $obj["response"];

        // Now request the HTML of the note using the slug
        $request2 = Request::createFromEnvironment($env);
        $request2 =
            $request2->withHeader("Content-Type", "application/json")
                ->withAttribute("slug", $slug);
        $response2 = new Response();

        $resp = $publicWebsite->getPublishedNote($request2, $response2);
        $this->assertTrue($resp->isOk());

        // Unpublish the note now
        $request3 = Request::createFromEnvironment($env);
        $request3 =
            $request3->withHeader("Content-Type", "application/json")
                ->withAttribute("id", $uuid);
        $response3 = new Response();

        $resp = $frontend->unpublishNote($request3, $response3);
        $this->assertTrue($resp->isSuccessful());

        // Now request the HTML of the note using the slug, must fail
        $request2 = Request::createFromEnvironment($env);
        $request2 =
            $request2->withHeader("Content-Type", "application/json")
                ->withAttribute("slug", $slug);
        $response2 = new Response();

        $resp = $publicWebsite->getPublishedNote($request2, $response2);
        $this->assertTrue($resp->isNotFound());
    }

    private function createSampleNote(): string {
        $uuid = "5964FD90-A74A-4E7B-A58D-3B3E84F88F07";
        $note = Note::createWithContents("Test", null, $uuid);
        $note->setContents("test");
        $this->getEntityManager()->persist($note);
        $this->getEntityManager()->flush();

        return $uuid;
    }
}
