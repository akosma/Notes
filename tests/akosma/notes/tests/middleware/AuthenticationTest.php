<?php
/**
 * Notes Application
 *
 * @author Adrian Kosmaczewski <learn@akosma.training>
 * @copyright 2017 Adrian Kosmaczewski
 * @license MIT
 *
 * Copyright © 2017. Adrian Kosmaczewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */

declare(strict_types=1);

/**
 * Notes
 * Created by akosma
 * Date: 29.08.17 22:24
 */

namespace akosma\notes\tests\middleware;

use akosma\notes\exceptions\ForbiddenAccessException;
use akosma\notes\helpers\Frontend;
use akosma\notes\middleware\Authentication;
use akosma\notes\tests\controllers\BaseControllerTest;
use Slim\Http\Environment;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Class SecurityTest
 *
 * @package akosma\notes\tests
 * @coversDefaultClass \akosma\notes\middleware\Authentication
 */
class AuthenticationTest extends BaseControllerTest {
    /**
     * @test
     */
    public function canAuthenticateUser() {
        $env = Environment::mock([
            "PHP_AUTH_USER" => "user",
            "PHP_AUTH_PW"   => "pass",
        ]);

        // Dummy callable to be used as a "next" for the authentication middleware
        $next = function (Request $request, Response $response) {
            return $response;
        };

        $request = Request::createFromEnvironment($env);
        $response = new Response();
        $auth = new Authentication($this->getContainer(), "user", "pass");
        $response = $auth($request, $response, $next);
        $this->assertTrue($response->isOk());
    }

    /**
     * @test
     */
    public function willNotAuthenticateUnauthorizedUser() {
        $env = Environment::mock([
            "PHP_AUTH_USER" => "nope",
            "PHP_AUTH_PW"   => "whatever",
        ]);
        $request = Request::createFromEnvironment($env);
        $response = new Response();
        $auth = new Authentication($this->getContainer(), "user", "pass");
        try {
            $auth($request, $response, Frontend::class . ":ping");
            $this->assertTrue(false);
        }
        catch (ForbiddenAccessException $exception) {
            $this->assertEquals(401, $exception->getStatusCode());
        }
    }
}
