<?php
/**
 * Notes Application
 *
 * @author Adrian Kosmaczewski <learn@akosma.training>
 * @copyright 2017 Adrian Kosmaczewski
 * @license MIT
 *
 * Copyright © 2017. Adrian Kosmaczewski
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */

declare(strict_types=1);

/**
 * Notes
 * Created by akosma
 * Date: 02.09.17 13:09
 */

namespace akosma\notes\tests\middleware;

use akosma\notes\middleware\Serializer;
use akosma\notes\tests\controllers\BaseControllerTest;
use CFPropertyList\CFPropertyList;
use Slim\Http\Environment;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Class SerializerTest
 *
 * @package akosma\notes\tests\middleware
 * @coversDefaultClass \akosma\notes\middleware\Serializer
 */
class SerializerTest extends BaseControllerTest {
    /**
     * @test
     */
    function rewritesResponseToBinaryPlist() {
        $env = Environment::mock([
            "CONTENT_TYPE" => "application/x-plist",
        ]);

        // Dummy callable to be used as a "next" for the authentication middleware
        $next = function (Request $request, Response $response) {
            return $response;
        };

        $obj = [
            "key1" => "value",
            "key2" => 2,
            "key3" => true,
            "key4" => [1, 2, 3],
        ];

        $request = Request::createFromEnvironment($env);
        $response = new Response();
        $response = $response->withJson($obj);
        $serializer = new Serializer($this->getContainer());
        $response = $serializer($request, $response, $next);
        $str = (string)$response->getBody();

        $plist = new CFPropertyList();
        $plist->parse($str, CFPropertyList::FORMAT_BINARY);
        $array = $plist->toArray();
        $this->assertEquals($array, $obj);
    }

    /**
     * @test
     */
    function rewritesResponseToXmlPlist() {
        $env = Environment::mock([
            "CONTENT_TYPE" => "application/xml",
        ]);

        // Dummy callable to be used as a "next" for the authentication middleware
        $next = function (Request $request, Response $response) {
            return $response;
        };

        $obj = [
            "key1" => "value",
            "key2" => 2,
            "key3" => true,
            "key4" => [1, 2, 3],
        ];

        $request = Request::createFromEnvironment($env);
        $response = new Response();
        $response = $response->withJson($obj);
        $serializer = new Serializer($this->getContainer());
        $response = $serializer($request, $response, $next);
        $str = (string)$response->getBody();

        $xml = <<< EOF
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
  <dict>
    <key>key1</key>
    <string>value</string>
    <key>key2</key>
    <integer>2</integer>
    <key>key3</key>
    <true/>
    <key>key4</key>
    <array>
      <integer>1</integer>
      <integer>2</integer>
      <integer>3</integer>
    </array>
  </dict>
</plist>

EOF;

        $this->assertEquals($xml, $str);
    }

    /**
     * @test
     */
    function rewritesResponseToXml() {
        $env = Environment::mock([
            "CONTENT_TYPE" => "text/xml",
        ]);

        // Dummy callable to be used as a "next" for the authentication middleware
        $next = function (Request $request, Response $response) {
            return $response;
        };

        $obj = [
            "key1" => "value",
            "key2" => 2,
            "key3" => true,
            "key4" => [1, 2, 3],
        ];

        $request = Request::createFromEnvironment($env);
        $response = new Response();
        $response = $response->withJson($obj);
        $serializer = new Serializer($this->getContainer());
        $response = $serializer($request, $response, $next);
        $str = (string)$response->getBody();

        $xml = <<< EOF
<?xml version="1.0" encoding="UTF-8"?>
<message>
  <key1>value</key1>
  <key2>2</key2>
  <key3>1</key3>
  <key4>
    <obj>1</obj>
    <obj>2</obj>
    <obj>3</obj>
  </key4>
</message>
EOF;

        $this->assertEquals($xml, $str);
    }

    /**
     * @test
     */
    function byDefaultReturnsJson() {
        $env = Environment::mock([]);

        // Dummy callable to be used as a "next" for the authentication middleware
        $next = function (Request $request, Response $response) {
            return $response;
        };

        $obj = [
            "key1" => "value",
            "key2" => 2,
            "key3" => true,
            "key4" => [1, 2, 3],
        ];

        $request = Request::createFromEnvironment($env);
        $response = new Response();
        $response = $response->withJson($obj);
        $serializer = new Serializer($this->getContainer());
        $response = $serializer($request, $response, $next);
        $str = (string)$response->getBody();

        $json = '{"key1":"value","key2":2,"key3":true,"key4":[1,2,3]}';
        $this->assertEquals($json, $str);
    }
}
