# Notes Application

This application replicates the functionality of [Simplenote](https://simplenote.com/). It can be installed in any hosting with [PHP 7.1](http://php.net/) or higher, and any database system supported by the [Doctrine ORM](http://doctrine-project.org/projects/orm.html). It is meant to be used by a single user.

## Requirements

This application is written as follows:

- **Server**: [PHP 7.1](http://php.net/) using `strict_types` enabled. API generated using [Slim Framework](https://www.slimframework.com/). Package management done via [Composer](https://getcomposer.org/). Deployment done with [Deployer](https://deployer.org/). Tests via [PHPUnit](https://phpunit.de/). Static code analysis via [Phan](https://github.com/phan/phan), [PHPMD](https://phpmd.org/) and [PHP Copy/Paste Detector](https://github.com/sebastianbergmann/phpcpd).
- **Database**: any database system supported by the [Doctrine ORM](http://doctrine-project.org/projects/orm.html): [MySQL](https://www.mysql.com/), [MariaDB](https://mariadb.org/), [Oracle](https://www.oracle.com/database/index.html), [SQL Server](https://www.microsoft.com/en-us/sql-server/default.aspx), [PostgreSQL](https://www.postgresql.org/), [SQLite](http://sqlite.org/), etc.
- **Client**: [TypeScript 2.5](http://www.typescriptlang.org/), compiled and minified with [webpack](https://webpack.js.org/). Package management done via [npm](https://www.npmjs.com/). CSS compiled and minified with [Less](http://lesscss.org/). Tests via [Mocha](http://mochajs.org/) using the [Chai](http://chaijs.com/) assertion library and the [Sinon](http://sinonjs.org/) spies, stubs and mocks library. Code coverage via [Istambul](https://istanbul.js.org/).

Code statistics are generated with [cloc](https://github.com/AlDanial/cloc) and [PHPLOC](https://github.com/sebastianbergmann/phploc).

Object serialization to and from XML and Property List is handled by [XML_Serializer](http://pear.php.net/package/XML_Serializer/) and [CFPropertyList](https://github.com/rodneyrehm/CFPropertyList).

## Installation

### Server Side Dependencies for macOS

Make sure you have installed the Xcode developer tools and [Homebrew](https://brew.sh/).

Install PHP 7.1 with the Xdebug extension:

    $ brew install homebrew/php/php71-xdebug
    $ brew install homebrew/php/php71-ast
    $ brew install phpunit
    $ brew install phpdocumentor
    $ brew install deployer
    $ brew install phpmd
    $ brew install cloc

Make sure your PHP installation is correct: `$ php --version` should return something like this:

    PHP 7.1.8 (cli) (built: Aug  7 2017 15:02:45) ( NTS )
    Copyright (c) 1997-2017 The PHP Group
    Zend Engine v3.1.0, Copyright (c) 1998-2017 Zend Technologies
        with Xdebug v2.5.5, Copyright (c) 2002-2017, by Derick Rethans

PHPMD should also be available by now: `$ phpmd --version` should show:

    PHPMD 2.6.0

The PHPUnit installation version, through `$ phpunit --version` should return:

    PHPUnit 6.3.0 by Sebastian Bergmann and contributors.

Install Composer locally in the project, as explained [in the Composer website](https://getcomposer.org/download/). This will bring a file named `composer.phar` in the root folder of the project. Make sure composer is installed correctly by running the following command: `$ php composer.phar --version` which should show this output:

    Composer version 1.5.1 2017-08-09 16:07:22

### Server Side Dependencies for Ubuntu Linux

Install the following packages:

    $ sudo apt-get install sqlite3
    $ sudo apt-get install php7.1-xdebug
    $ sudo apt-get install php7.1-dom
    $ sudo apt-get install php-ast
    $ sudo apt-get install php-mbstring
    $ sudo apt-get install php7.1-sqlite
    $ sudo apt-get install php7.1-mysql

Follow the instructions on the [PHPUnit website](https://phpunit.de/getting-started.html) to install PHPUnit:

1. `wget https://phar.phpunit.de/phpunit.phar`
2. `chmod +x phpunit.phar`
3. `sudo mv phpunit.phar /usr/local/bin/phpunit`

Similarly, to install [PhpDocumentor](https://phpdoc.org/) just follow these steps:

1. `wget http://phpdoc.org/phpDocumentor.phar`
2. `chmod +x phpDocumentor.phar`
3. `sudo mv phpDocumentor.phar /usr/local/bin/phpdoc`

### Client Side Dependencies

Install [Node](https://nodejs.org/en/) (which bundles the the Node Package Manager [npm](https://www.npmjs.com/) with it) using Homebrew.

    $ brew install node

Then install the following libraries in your system:

    $ npm install --global mocha
    $ npm install --global istanbul
    $ npm install --global nyc
    $ npm install --global typedoc
    $ npm install --global webpack
    $ npm install --global ts-loader
    $ npm install --global less
    $ npm install --global less-plugin-clean-css

### Package Managers

The project uses both Composer and npm as component managers. You must update both in order to download the required libraries locally.

    $ php composer.phar install
    $ npm install

This will create two folders in the root of the project:

- `vendor` which contains the PHP libraries, as specified in `composer.json`.
- `node_modules` which contains the JavaScript libraries, as specified in `package.json`.

None of these two folders will be checked into Git.

### Run Application Locally

Once the libraries above are installed, follow these steps to run the application locally:

1. The root web folder for the application is the `public` folder. Make sure your web server uses this as the default root for the application.
2. Copy the file `config/config.sample.ini` and name it `config/config.ini`. Change the `url` parameter of the database to match your setup (database name, server, username, password, etc). The configuration of the `url` parameter is explained in the [Doctrine documentation](http://docs.doctrine-project.org/projects/doctrine-dbal/en/latest/reference/configuration.html). **Do not save the `config.ini` file in your Git repository.** By default, the configuration file uses a SQLite database called `/tmp/notes.sqlite`.
3. Create the database schema using the following Doctrine command: `$ php vendor/bin/doctrine orm:schema-tool:create`. This command **requires PHP 7.1**.
4. Test the API using the following [curl](https://curl.haxx.se/) command: `$ curl http://localhost:8888/api/ping` which should return the following (replace `localhost:8888` by your local URL):

```
{"response":true,"version":"v1","timestamp":"2017-08-29T10:50:02+0200"}
```

The API endpoints require a username and password: by default, they are `notes` and `password`. You can change them in the `config/config.ini` file. As noted previously, this application is meant to be used by a single user.

## PhpStorm

This project can be opened in [PhpStorm](http://www.jetbrains.com/phpstorm/). Make sure to setup your project following these guidelines:

1. **Preferences > Languages & Frameworks > PHP**: PHP language level 7.1. Select your local interpreter (the one you installed with Homebrew above.)
2. **Preferences > Languages & Frameworks > PHP > Composer**: select the `composer.json` file located in the root of this project. Also download a copy of `composer.phar` to be installed in the root of the project. Do not worry, it will not be included in the Git repository.
3. **Preferences > Languages & Frameworks > PHP > Test Frameworks**: Add a PHP Unit Local configuration, using the Composer autoloader. Make sure that the version of PHPUnit shown is 6 or higher. Select the `vendor/autoload.php` file located in your project (which was automatically generated when Composer autoupdated itself), and select the default configuration file to be the `phpunit.xml` file at the root of the project.

You should be now able to run the server-side unit tests directly from PhpStorm:

1. Select the menu **Run > Edit Configurations…**
2. Create a new PHPUnit configuration. Give it the name "server tests" and select "Defined in the configuration file."
3. Select the menu **Run > Run 'server tests'** and see the tests run.
4. To see the code coverage, select **Run > Run 'server tests' with Coverage**

To run the client-side unit tests directly from PhpStorm:

1. Select the menu **Run > Edit Configurations…**
2. Create a new Mocha configuration. Give it the name "client tests" and leave all the defaults. Select "All in directory" and in the field "Test directory" browse to your `public/tests` folder from the root of the project.
3. Select the menu **Run > Run 'client tests'** and see the tests run.
4. To see the code coverage, select **Run > Run 'client tests' with Coverage**

## Compile JavaScript automatically

The client application is build on TypeScript, which must be compiled into JavaScript prior to execution. If you modify your TypeScript files, you must remember to recompile them, which is easy to forget.

To solve this problem, always keep a terminal open during development and run the following command on it:

    $ npm run watch

This will run the `webpack --watch` command, which in turn recompiles automatically all JavaScript and TypeScript files into a non-minified version of `public/src/bundle.js`, which is referenced by the main `index.html` file of the application.

## Compatibility

Thanks to [w2ui](http://w2ui.com/web/), the web application is compatible with the following desktop browsers:

- Chrome
- Firefox 7+
- Safari 5+
- IE 9+
- Opera

## Tests

The server-side tests are based on [PHPUnit](https://phpunit.de/) 6 or higher. They use a special Doctrine configuration, using an in-memory SQLite database, and thus they do not require a running database server to run.

- The PHPUnit tests can be run from the root of the project using the command `$ phpunit`. The file `phpunit.xml` contains all the configuration parameters required by PHPUnit to run.
- The Mocha/Chai tests (JavaScript) can be run from the root of the project using the command `$ mocha public/tests`.
- You can run all tests (both server and client) in one shot using the `$ make test` command.

## Makefile

The `Makefile` in the root of the project contains shortcuts to various actions:

- `$ make docs` will generate a `_build` folder in the root of the project, with the documentation generated with [phpDocumentor](https://www.phpdoc.org/) and [TypeDoc](http://typedoc.org/).
- `$ make remote-test` will use [Deployer](https://deployer.org/) to run the unit tests in the remote server.
- `$ make test` will run the unit tests (both server and client) in the console locally.
- `$ make deploy` will run various steps to deploy the latest version of the code in the server.

Please refer to the file `Makefile` for more information.

## Deployment

This project uses [Deployer](https://deployer.org/) to streamline the deployment process. Configure your own remote server using the `deploy.php` file provided in the root of the project. This will require SSH access to your remote server, and also SSH access to [Github](https://github.com/) or [BitBucket](https://bitbucket.org/) to your project from the server. Make sure all keys are correctly installed.

## Telegram Bot Webhook

This application can listen to the [Telegram Bot API](https://core.telegram.org/bots) as a dedicated webhook, so that notes can be created directly from a Telegram Bot.

1. Follow the instructions to create a dedicated bot using the [BotFather bot](https://telegram.me/botfather) and the `/newbot` command directly from Telegram.
2. Generate a "secret" part of your webhook URL using a command such as `uuidgen | shasum5.18 | pbcopy` in macOS.
3. Store that secret in the `config.ini` file.
4. Register the webhook using the Telegram API: `curl https://api.telegram.org/bot[BOT_API_KEY]/setWebhook\?url\=https://[YOUR_URL]/api/telegram/[SECRET]`
